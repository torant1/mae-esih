<?php

namespace mae_esih\Http\Controllers;

use mae_esih\model\PagesModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$uri = Pages::findOrFail($pagesname);

         //return  view('pages.'.$pagesname);
        
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \mae_esih\model\PagesModel  $pagesModel
     * @return \Illuminate\Http\Response
     */
    public function show($slug='index')
    {   
              $pageslug = 'pages.' . (string)$slug; 
                // This means that your views must be in views/page/ folder
                if( view()->exists($pageslug)){
                    return view($pageslug);
                }
                abort(404);
            
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \mae_esih\model\PagesModel  $pagesModel
     * @return \Illuminate\Http\Response
     */
    public function edit(PagesModel $pagesModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \mae_esih\model\PagesModel  $pagesModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PagesModel $pagesModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \mae_esih\model\PagesModel  $pagesModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(PagesModel $pagesModel)
    {
        //
    }
}
