@extends('layouts.master_home')
@section('content')
		<div class="main-page-wrapper about-us-v1-page">


			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2>404</h2>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


		

			<!-- Error Page _________________________ -->
			<div class="error-page">
				<div class="container">
					<div class="error-wrapper clear-fix">
						<img src="images/inner-page/6.jpg" alt="" class="float-left wow fadeInLeft">
						<div class="text float-right wow fadeInRight">
							<h2>404 <span class="p-color opps"></span> <span class="last p-color">ERROR!</span></h2>
							<p>cette page n'existe pas.</p>
							<a href="index" class="tran3s"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Page d'accueil</a>
						</div> <!-- /.text -->
					</div> <!-- /.error-wrapper -->
				</div> <!-- /.container -->
			</div> <!-- /.error-page -->




@endsection