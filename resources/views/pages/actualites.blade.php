@extends('layouts.master_home')
@section('content')
	        <!-- Latest News ___________________________ -->
	        	<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2 class="text-center">DERNIERES ACTUALITES </h2>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
					<ul>
						<li><a href="index">Accueil</a></li>
						<li><li><i class="fa fa-caret-right"></i></li></li>
						<li>Actualit&eacute;s</li>
						<li><li><i class="fa fa-caret-right"></i></li></li>
						<li>Actualit&eacute;s</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->

	        <div class="latest-news wow fadeInUp theme-bg-color">
	        	<div class="container">
	        		<div class="theme-title">
	        			<h2>Dernieres Actualites</h2>
	        			<p>Something for education news,latest news feed</p>
	        		</div>

	        		<div class="post-wrapper row">
	        			<div class="single-post wow fadeInUp col-lg-4 col-md-4 col-sm-6 col-xs-6">
	        					<div class="img-holder">
	        						<div class="date wow fadeInUp p-color-bg">12 <span>Sep</span></div>
	        						<img src="images/blog/1.jpg" alt="Image">
	        						<a href="blog-details.html" class="tran4s"></a>
	        					</div>
	        					<div class="text-wrapper">
	        						<div class="text tran4s">
	        							<a href="blog-details.html">New Chicago school budget </a>
	        							<p>Education is the process of facilitating learning. Knowledge kills, values, beliefs, and habits of a group of people are trans- ferred  </p>
	        						</div> <!-- /.text -->
	        					</div> <!-- /.text-wrapper -->
	        			</div> <!-- /.single-post -->

	        			<div class="single-post wow fadeInUp col-lg-4 col-md-4 col-sm-6 col-xs-6">
	        					<div class="img-holder">
	        						<div class="date wow fadeInUp p-color-bg">12 <span>Sep</span></div>
	        						<img src="images/blog/2.jpg" alt="Image">
	        						<a href="blog-details.html" class="tran4s"></a>
	        					</div>
	        					<div class="text-wrapper">
	        						<div class="text tran4s">
	        							<a href="blog-details.html">New Chicago school budget </a>
	        							<p>Education is the process of facilitating learning. Knowledge kills, values, beliefs, and habits of a group of people are trans- ferred  </p>
	        						</div> <!-- /.text -->
	        					</div> <!-- /.text-wrapper -->
	        			</div> <!-- /.single-post -->

	        			<div class="single-post wow fadeInUp col-lg-4 col-md-4 col-sm-6 col-xs-6">
	        					<div class="img-holder">
	        						<div class="date wow fadeInUp p-color-bg">12 <span>Sep</span></div>
	        						<img src="images/blog/3.jpg" alt="Image">
	        						<a href="blog-details.html" class="tran4s"></a>
	        					</div>
	        					<div class="text-wrapper">
	        						<div class="text tran4s">
	        							<a href="blog-details.html">New Chicago school budget </a>
	        							<p>Education is the process of facilitating learning. Knowledge kills, values, beliefs, and habits of a group of people are trans- ferred  </p>
	        						</div> <!-- /.text -->
	        					</div> <!-- /.text-wrapper -->
	        			</div> <!-- /.single-post -->
	        		</div> <!-- /.post-wrapper -->
	        	</div> <!-- /.container -->
	        </div> <!-- /.latest-news -->

@endsection