@extends('layouts.master_home')
@section('content')
			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2>Faq</h2>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
					<ul>
						<li><a href="index.html">Accueil</a></li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Faq</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->


			<!-- Faq Page ____________________________ -->
			<div class="faq-page faq">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 wow fadeInleft">
							<h4>for Courses faq </h4>
							<!-- ________________ Panel _______________ -->
	        					<div class="faq_panel">
									<div class="panel-group theme-accordion" id="accordion">
									  <div class="panel">
									    <div class="panel-heading active-panel">
									      <h6 class="panel-title">
									        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
									        Smart Learning is an education platform partners</a>
									      </h6>
									    </div>
									    <div id="collapse1" class="panel-collapse collapse in">
									      <div class="panel-body">
									      	<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempo- ra incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam voluptatem</p>
									      </div>
									    </div>
									  </div> <!-- /panel 1 -->
									  <div class="panel">
									    <div class="panel-heading">
									      <h6 class="panel-title">
									        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
									         Education of Tomorrow, Rooted in Tradition</a>
									      </h6>
									    </div>
									    <div id="collapse2" class="panel-collapse collapse">
									      <div class="panel-body">
									      	<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam voluptatem</p>
									      </div>
									    </div>
									  </div> <!-- /panel 2 -->
									  <div class="panel">
									    <div class="panel-heading">
									      <h6 class="panel-title">
									        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
									        Lorem ipsum dolor sit amet, consectetur adipiscing</a>
									      </h6>
									    </div>
									    <div id="collapse3" class="panel-collapse collapse">
									      <div class="panel-body">
									      	<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam voluptatem</p>
									      </div>
									    </div>
									  </div> <!-- /panel 3 -->

									  <div class="panel">
									    <div class="panel-heading">
									      <h6 class="panel-title">
									        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
									        Build Education Website Using HTML</a>
									      </h6>
									    </div>
									    <div id="collapse4" class="panel-collapse collapse">
									      <div class="panel-body">
									      	<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam voluptatem</p>
									      </div>
									    </div>
									  </div> <!-- /panel 4 -->

									  <div class="panel">
									    <div class="panel-heading">
									      <h6 class="panel-title">
									        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
									        BEST INDUSTRY LEADERS for awesome courses</a>
									      </h6>
									    </div>
									    <div id="collapse5" class="panel-collapse collapse">
									      <div class="panel-body">
									      	<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam voluptatem</p>
									      </div>
									    </div>
									  </div> <!-- /panel 5 -->

									</div> <!-- end #accordion -->
								</div> <!-- End of .faq_panel -->
						</div>
						<!--  -->
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 wow fadeInRight">
							<h4>for Courses faq </h4>
							<!-- ________________ Panel _______________ -->
	        					<div class="faq_panel">
									<div class="panel-group theme-accordion" id="accordiontwo">
									  <div class="panel">
									    <div class="panel-heading">
									      <h6 class="panel-title">
									        <a data-toggle="collapse" data-parent="#accordiontwo" href="#collapse21">
									        Smart Learning is an education platform partners</a>
									      </h6>
									    </div>
									    <div id="collapse21" class="panel-collapse collapse">
									      <div class="panel-body">
									      	<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempo- ra incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam voluptatem</p>
									      </div>
									    </div>
									  </div> <!-- /panel 1 -->
									  <div class="panel">
									    <div class="panel-heading">
									      <h6 class="panel-title">
									        <a data-toggle="collapse" data-parent="#accordiontwo" href="#collapse22">
									         Education of Tomorrow, Rooted in Tradition</a>
									      </h6>
									    </div>
									    <div id="collapse22" class="panel-collapse collapse">
									      <div class="panel-body">
									      	<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam voluptatem</p>
									      </div>
									    </div>
									  </div> <!-- /panel 2 -->
									  <div class="panel">
									    <div class="panel-heading">
									      <h6 class="panel-title">
									        <a data-toggle="collapse" data-parent="#accordiontwo" href="#collapse23">
									        Lorem ipsum dolor sit amet, consectetur adipiscing</a>
									      </h6>
									    </div>
									    <div id="collapse23" class="panel-collapse collapse">
									      <div class="panel-body">
									      	<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam voluptatem</p>
									      </div>
									    </div>
									  </div> <!-- /panel 3 -->

									  <div class="panel">
									    <div class="panel-heading">
									      <h6 class="panel-title">
									        <a data-toggle="collapse" data-parent="#accordiontwo" href="#collapse24">
									        Build Education Website Using HTML</a>
									      </h6>
									    </div>
									    <div id="collapse24" class="panel-collapse collapse">
									      <div class="panel-body">
									      	<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam voluptatem</p>
									      </div>
									    </div>
									  </div> <!-- /panel 4 -->

									  <div class="panel">
									    <div class="panel-heading">
									      <h6 class="panel-title">
									        <a data-toggle="collapse" data-parent="#accordiontwo" href="#collapse25">
									        BEST INDUSTRY LEADERS for awesome courses</a>
									      </h6>
									    </div>
									    <div id="collapse25" class="panel-collapse collapse">
									      <div class="panel-body">
									      	<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam voluptatem</p>
									      </div>
									    </div>
									  </div> <!-- /panel 5 -->

									  <div class="panel">
									    <div class="panel-heading">
									      <h6 class="panel-title">
									        <a data-toggle="collapse" data-parent="#accordiontwo" href="#collapse26">
									        BEST INDUSTRY LEADERS for awesome courses</a>
									      </h6>
									    </div>
									    <div id="collapse26" class="panel-collapse collapse">
									      <div class="panel-body">
									      	<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam voluptatem</p>
									      </div>
									    </div>
									  </div> <!-- /panel 6 -->

									  <div class="panel">
									    <div class="panel-heading">
									      <h6 class="panel-title">
									        <a data-toggle="collapse" data-parent="#accordiontwo" href="#collapse27">
									        BEST INDUSTRY LEADERS for awesome courses</a>
									      </h6>
									    </div>
									    <div id="collapse27" class="panel-collapse collapse">
									      <div class="panel-body">
									      	<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam voluptatem</p>
									      </div>
									    </div>
									  </div> <!-- /panel 7 -->

									  <div class="panel">
									    <div class="panel-heading">
									      <h6 class="panel-title">
									        <a data-toggle="collapse" data-parent="#accordiontwo" href="#collapse28">
									        BEST INDUSTRY LEADERS for awesome courses</a>
									      </h6>
									    </div>
									    <div id="collapse28" class="panel-collapse collapse">
									      <div class="panel-body">
									      	<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam voluptatem</p>
									      </div>
									    </div>
									  </div> <!-- /panel 8 -->

									</div> <!-- end #accordion -->
								</div> <!-- End of .faq_panel -->
						</div>
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.faq-page -->




			<!-- SubsCribe Banner ___________________ -->
	        <div class="subscribe-banner p-color-bg wow fadeInUp">
	        	<div class="container">
	        		<h3>Subscribe now</h3>
	        		<p>Receive weekly newsletter with educational materials, new courses, most popular posts, popular books and much more!</p>
	        		<form action="#" class="clear-fix">
	        			<input type="email" class="float-left wow fadeInLeft" placeholder="Email address">
	        			<button class="float-left tran3s wow fadeInRight">Subcribe</button>
	        		</form>
	        	</div>
	        </div>

@endsection