@extends('layouts.master_home')
@section('content')
			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2 class="text-center">VENEZ SOUTENIR L’EXCELLENCE</h2>
						<h4 class="text-inner-second">Investissons ensemble dans l’Ha&iuml;ti de demain</h4>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
					<ul class="float-left">
						<li><a href="index">Home</a></li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>A propos</li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Partenaires</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->

			<!-- Event Section _______________________ -->
	        <div class="partenaire-section wow fadeInUp">
	        	<div class="container">
					<div class="row">
						<div class="text-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<h2 class=" text-bold text-title">En soutenant LE MAE_ESIH</h2>
							<p>
								<ul>
								    <li><span class=" text-list">Vous investissez dans une recherche et une formation de qualité au service D’Ha&iuml;ti.</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="1s"><span class=" text-list">Vous contribuez activement &agrave; une nouvelle cat&eacute;gorie de dirigeants et de managers  pour le développement  des Entreprises haïtiennes et de leur rayonnement &agrave; l'international.</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="2s"><span class=" text-list">Vous participez à la construction de passerelles entre le programme MAE-DE et le monde socio-économique.</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="3s"><span class=" text-list">Vous apportez votre soutien au développement du MAE-DE de l’ESIH</span></li>
								</ul>
							</p>
						<br>
					
						</div>
					    			
					</div>    		
	        	</div>
	        </div>
	  
@endsection