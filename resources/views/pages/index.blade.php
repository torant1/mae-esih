@extends('layouts.master_home')
@section('content')
		<div class="main-page-wrapper">
			<!-- Theme Banner ________________________________ -->
			<div id="banner">
	        	<div class="rev_slider_wrapper">
					<!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
						<div id="main-banner-slider" class="rev_slider" data-version="5.0.7" >
							<ul>

								<!-- SLIDE1  -->
								<li data-index="rs-280" data-transition="zoomin" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-saveperformance="off"  data-title="l'excellence manageriale au coeur d'haiti" data-description="">
									<!-- MAIN IMAGE -->
									<img src="https://i.pinimg.com/originals/8b/2d/59/8b2d598bf6e9d9e8707a44537c2ac5e4.png"  alt="image"  class="rev-slidebg" data-bgparallax="10" data-bgposition="center center" data-duration="30000" data-ease="Linear.easeNone" data-kenburns="on" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0" data-rotateend="0" data-rotatestart="0"  data-scalestart="100" data-scaleend="140">
									<!-- LAYERS -->

									<!-- LAYER NR. 1 -->
									<div class="tp-caption" 
										data-x="['left','left','left','left']" data-hoffset="['0','25','35','15']" 
										data-y="['middle','middle','middle','middle']" data-voffset="['-56','-56','-45','-150']" 
										data-width="none"
										data-height="none"
										data-whitespace="nowrap"
										data-transform_idle="o:1;"
							 
										data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
										data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
										data-mask_in="x:0px;y:[100%];" 
										data-mask_out="x:inherit;y:inherit;" 
										data-start="1000" 
										data-splitin="none" 
										data-splitout="none" 
										data-responsive_offset="on" 
										style="z-index: 6; white-space: nowrap;">
										{{-- <h5>for hundreds of successful student here </h5> --}}
									</div>

									<!-- LAYER NR. 2 -->
									<div class="tp-caption" 
										data-x="['left','left','left','left']" data-hoffset="['0','25','35','15']" 
										data-y="['middle','middle','middle','middle']" data-voffset="['20','25','30','-85']"
										data-width="none"
										data-height="none"
										data-whitespace="nowrap"
										data-transform_idle="o:1;"
							 
										data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
										data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
										data-mask_in="x:0px;y:[100%];" 
										data-mask_out="x:inherit;y:inherit;" 
										data-start="2000" 
										data-splitin="none" 
										data-splitout="none" 
										data-responsive_offset="on" 
										style="z-index: 6; white-space: nowrap; ">
										<h1 class="center-item">Le  <span class="p-color">management</span> de l’entreprise du XXIè siècle.</h1>
									</div>


									<!-- LAYER NR. 3 -->
									<div class="tp-caption"
										data-x="['left','left','left','left']" data-hoffset="['0','25','35','15']" 
										data-y="['middle','middle','middle','middle']" data-voffset="['205','205','210','80']"
										data-transform_idle="o:1;"
										data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
										data-transform_in="x:[-100%];z:0;rX:0deg;rY:0deg;rZ:0deg;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2500;e:Power3.easeInOut;" 
										data-transform_out="auto:auto;s:1000;e:Power2.easeInOut;" 
										data-start="3000" 
										data-splitin="none" 
										data-splitout="none" 
										data-responsive_offset="on">
{{-- 										<a href="course-v1" class="course-button">View courses</a>
 --}}									</div>
									
									<!-- LAYER NR. 4 -->
									<div class="tp-caption"
										data-x="['left','left','left','left']" data-hoffset="['192','217','227','15']" 
										data-y="['middle','middle','middle','middle']" data-voffset="['205','205','210','155']"
										data-transform_idle="o:1;"
										data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
										data-transform_in="x:[100%];z:0;rX:0deg;rY:0deg;rZ:0deg;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2500;e:Power3.easeInOut;" 
										data-transform_out="auto:auto;s:1000;e:Power2.easeInOut;" 
										data-start="3000" 
										data-splitin="none" 
										data-splitout="none" 
										data-responsive_offset="on">
{{-- 										<a href="contact-us.html" class="buy-button p-color-bg">BUY NOW</a>
 --}}									</div>
								
								</li>


								<!-- SLIDE2  -->
								<li data-index="rs-18" data-transition="zoomout" data-slotamount="7"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="hhttps://i.pinimg.com/564x/a6/ec/1a/a6ec1a492c9a19dc39e052df96700ff2.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="un diplome de standard international" data-description="">
									<!-- MAIN IMAGE -->
									<img src="https://i.pinimg.com/564x/a6/ec/1a/a6ec1a492c9a19dc39e052df96700ff2.jpg"  alt=""  data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="120" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
									<!-- LAYERS -->

									<!-- LAYER NR. 1 -->
									<div class="tp-caption" 
										data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
										data-y="['middle','middle','middle','middle']" data-voffset="['-56','-56','-36','-150']" 
										data-width="none"
										data-height="none"
										data-whitespace="nowrap"
										data-transform_idle="o:1;"
							 
										data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
										data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
										data-mask_in="x:0px;y:[100%];" 
										data-mask_out="x:inherit;y:inherit;" 
										data-start="1000" 
										data-splitin="none" 
										data-splitout="none" 
										data-responsive_offset="on" 
										style="z-index: 6; white-space: nowrap;">
{{-- 										<h5>for hundreds of successful student here </h5>
 --}}									</div>

									<!-- LAYER NR. 2 -->
									<div class="tp-caption" 
										data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
										data-y="['middle','middle','middle','middle']" data-voffset="['20','20','45','-85']"
										data-width="none"
										data-height="none"
										data-whitespace="nowrap"
										data-transform_idle="o:1;"
							 
										data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
										data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
										data-mask_in="x:0px;y:[100%];" 
										data-mask_out="x:inherit;y:inherit;" 
										data-start="2000" 
										data-splitin="none" 
										data-splitout="none" 
										data-responsive_offset="on" 
										style="z-index: 6; white-space: nowrap;">
										<h1 class="center-item">un <span class="p-color">diplome</span> de standard  <span class="p-color">international</span></h1>
									</div>


									<!-- LAYER NR. 3 -->
									<div class="tp-caption"
										data-x="['center','center','center','center']" data-hoffset="['-87','-87','-87','0']" 
										data-y="['middle','middle','middle','middle']" data-voffset="['205','205','210','80']"
										data-transform_idle="o:1;"
										data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
										data-transform_in="x:[-100%];z:0;rX:0deg;rY:0deg;rZ:0deg;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2500;e:Power3.easeInOut;" 
										data-transform_out="auto:auto;s:1000;e:Power2.easeInOut;" 
										data-start="3000" 
										data-splitin="none" 
										data-splitout="none" 
										data-responsive_offset="on">
{{-- 										<a href="course-v1.html" class="course-button">View courses</a>
 --}}									</div>
									
									<!-- LAYER NR. 4 -->
									<div class="tp-caption"
										data-x="['center','center','center','center']" data-hoffset="['105','105','105','0']" 
										data-y="['middle','middle','middle','middle']" data-voffset="['205','205','210','155']"
										data-transform_idle="o:1;"
										data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
										data-transform_in="x:[100%];z:0;rX:0deg;rY:0deg;rZ:0deg;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2500;e:Power3.easeInOut;" 
										data-transform_out="auto:auto;s:1000;e:Power2.easeInOut;" 
										data-start="3000" 
										data-splitin="none" 
										data-splitout="none" 
										data-responsive_offset="on">
{{-- 										<a href="contact-us.html" class="buy-button p-color-bg">BUY NOW</a>
 --}}									</div>
								</li>

							
								<!-- SLIDE3  -->
								<li data-index="rs-20" data-transition="zoomin" data-slotamount="7"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="https://i.pinimg.com/originals/71/33/50/713350a09a24899d2aa8f79080b870d2.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Une formation alliant innovation" data-description="">
									<!-- MAIN IMAGE -->
									<img src="https://i.pinimg.com/originals/71/33/50/713350a09a24899d2aa8f79080b870d2.jpg"  alt=""  data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 -500" data-offsetend="0 500" data-bgparallax="10" class="rev-slidebg" data-no-retina>
									<!-- LAYERS -->

									<!-- LAYER NR. 1 -->
									<div class="tp-caption" 
										data-x="['left','left','left','left']" data-hoffset="['0','25','35','15']" 
										data-y="['middle','middle','middle','middle']" data-voffset="['-56','-56','-45','-150']" 
										data-width="none"
										data-height="none"
										data-whitespace="nowrap"
										data-transform_idle="o:1;"
							 
										data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
										data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
										data-mask_in="x:0px;y:[100%];" 
										data-mask_out="x:inherit;y:inherit;" 
										data-start="1000" 
										data-splitin="none" 
										data-splitout="none" 
										data-responsive_offset="on" 
										style="z-index: 6; white-space: nowrap;">
										{{-- <h5>for hundreds of successful student here </h5> --}}
									</div>

									<!-- LAYER NR. 2 -->
									<div class="tp-caption" 
										data-x="['left','left','left','left']" data-hoffset="['0','25','35','15']" 
										data-y="['middle','middle','middle','middle']" data-voffset="['20','25','30','-85']"
										data-width="none"
										data-height="none"
										data-whitespace="nowrap"
										data-transform_idle="o:1;"
							 
										data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
										data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
										data-mask_in="x:0px;y:[100%];" 
										data-mask_out="x:inherit;y:inherit;" 
										data-start="2000" 
										data-splitin="none" 
										data-splitout="none" 
										data-responsive_offset="on" 
										style="z-index: 6; white-space: nowrap;">
										<h1>Une formation alliant <span class="p-color">innovation</span> et
										<br> <span class="p-color">performance</span> pour penser ensemble le monde</h1>
									</div>


									<!-- LAYER NR. 3 -->
									<div class="tp-caption"
										data-x="['left','left','left','left']" data-hoffset="['0','25','35','15']" 
										data-y="['middle','middle','middle','middle']" data-voffset="['205','205','210','80']"
										data-transform_idle="o:1;"
										data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
										data-transform_in="x:[-100%];z:0;rX:0deg;rY:0deg;rZ:0deg;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2500;e:Power3.easeInOut;" 
										data-transform_out="auto:auto;s:1000;e:Power2.easeInOut;" 
										data-start="3000" 
										data-splitin="none" 
										data-splitout="none" 
										data-responsive_offset="on">
										{{-- <a href="course-v1.html" class="course-button">View courses</a> --}}
									</div>
									
									<!-- LAYER NR. 4 -->
									<div class="tp-caption"
										data-x="['left','left','left','left']" data-hoffset="['192','217','227','15']" 
										data-y="['middle','middle','middle','middle']" data-voffset="['205','205','210','155']"
										data-transform_idle="o:1;"
										data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
										data-transform_in="x:[100%];z:0;rX:0deg;rY:0deg;rZ:0deg;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2500;e:Power3.easeInOut;" 
										data-transform_out="auto:auto;s:1000;e:Power2.easeInOut;" 
										data-start="3000" 
										data-splitin="none" 
										data-splitout="none" 
										data-responsive_offset="on">
{{-- 										<a href="contact-us.html" class="buy-button p-color-bg">BUY NOW</a>
 --}}									</div>
								</li>
							</ul>	
						</div>
					</div><!-- END REVOLUTION SLIDER -->
	        </div> <!--  /#banner -->


	        <!-- Manage Section _________________________ -->
	        <div class="theme-manage-area theme-bg-color">
	        	<div class="container">
	        		<div class="text-content item-part float-left  p-color-bg col-lg-4 col-md-4 col-sm-12 col-xs-12">
	        			<h3><i class="fa fa-graduation-cap" aria-hidden="true"></i> Espace Carrieres</h3>
	        			<p>Le MAE-DE un vivier pour recruter vos futurs collaborateurs</p>
	        			<a href="mae-carrieres-emploi" class="tran3s">Lire plus <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
	        		</div>
	        		<div class="item-part float-left  youtub_object col-lg-4 col-md-4 col-sm-12 col-xs-12">
	        			<div class="video-wrapper" style="height: 100%;">
		        							<div class="video-container" style="height: 100%;">
		        							<iframe src="https://www.youtube-nocookie.com/embed/XdyLp0_-r0s?rel=0" allowfullscreen="allowfullscreen"  style="width: 100%; " frameborder="0" ></iframe>

		        							</div>
		        		</div>

	        		</div>
	        	
	        		<div class="text-content item-part float-left  p-color-bg col-lg-4 col-md-4 col-sm-12 col-xs-12">
	        			<h3><i class="fa fa-book" aria-hidden="true"></i> Admission</h3>
	        			<p>Les conditions d'admission</p>
	        			<a href="admission-mae" class="tran3s">Lire plus <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
	        		</div>
	        	</div> <!-- /.container -->
	        </div> <!-- /.theme-manage-area -->


	        <!-- Popular Course _________________________ -->
	       {{--  <div class="popular-course wow fadeInUp theme-bg-color">
	        	<div class="container">
	        	<div class=" col-10 col-md-10 col-sm-12 col-xs-12">
	        		<h2>Actualites a la Une</h2>
		        	<div class="row">
		        		<div class="theme-slider course-item-wrapper">
		        			<div class="item hvr-float-shadow">
		        				<div class="img-holder"><img src="images/home/2.jpg" alt="Image"></div>
		        				<div class="text">
		        					<h4><a href="course-details.html">web development courses</a></h4>		        					
		        					<p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat</p>
		        					<div class="clear-fix">
		        						<ul class="float-left">
		        							<li><i class="fa fa-comment" aria-hidden="true"></i> 5</li>
		        							<li><i class="fa fa-heart" aria-hidden="true"></i> 4</li>
		        						</ul>
		        					</div>
		        				</div> <!-- /.text -->
		        			</div> <!-- /.item -->

		        			<div class="item hvr-float-shadow">
		        				<div class="img-holder"><img src="images/home/3.jpg" alt="Image"></div>
		        				<div class="text">
		        					<h4><a href="course-details.html">Learn Basic German Fast</a></h4>
		        					<p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat</p>
		        					<div class="clear-fix">
		        						<ul class="float-left">
		        							<li><i class="fa fa-comment" aria-hidden="true"></i> 5</li>
		        							<li><i class="fa fa-heart" aria-hidden="true"></i> 3</li>
		        						</ul>
		        					</div>
		        				</div> <!-- /.text -->
		        			</div> <!-- /.item -->

		        			<div class="item hvr-float-shadow">
		        				<div class="img-holder"><img src="images/home/4.jpg" alt="Image"></div>
		        				<div class="text">
		        					<h4><a href="course-details.html">basic time use properly</a></h4>
		        					<p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat</p>
		        					<div class="clear-fix">
		        						<ul class="float-left">
		        							<li><i class="fa fa-comment" aria-hidden="true"></i> 5</li>
		        							<li><i class="fa fa-heart" aria-hidden="true"></i> 4</li>
		        						</ul>
		        					</div>
		        				</div> <!-- /.text-->
		        			</div> <!-- /.item -->

		        			<div class="item hvr-float-shadow">
		        				<div class="img-holder"><img src="images/home/3.jpg" alt="Image"></div>
		        				<div class="text">
		        					<h4><a href="course-details.html">Learn Basic German Fast</a></h4>
		        					<p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat</p>
		        					<div class="clear-fix">
		        						<ul class="float-left">
		        							<li><i class="fa fa-comment" aria-hidden="true"></i> 5</li>
		        							<li><i class="fa fa-heart" aria-hidden="true"></i> 4</li>
		        						</ul>
		        					</div>
		        				</div> 
		        			</div> <!-- /.item -->

		        			<div class="item hvr-float-shadow">
		        				<div class="img-holder"><img src="images/home/2.jpg" alt="Image"></div>
		        				<div class="text">
		        					<h4><a href="course-details.html">web development courses</a></h4>
		        					<p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat</p>
		        					<div class="clear-fix">
		        						<ul class="float-left">
		        							<li><i class="fa fa-comment" aria-hidden="true"></i> 5</li>
		        							<li><i class="fa fa-heart" aria-hidden="true"></i> 5</li>
		        						</ul>
		        					</div>
		        				</div> <!-- /.text -->
		        			</div> <!-- /.item -->

		        			<div class="item hvr-float-shadow">
		        				<div class="img-holder"><img src="images/home/2.jpg" alt="Image"></div>
		        				<div class="text">
		        					<h4><a href="course-details.html">web development courses</a></h4>
		        					<p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat</p>
		        					<div class="clear-fix">
		        						<ul class="float-left">
		        							<li><i class="fa fa-comment" aria-hidden="true"></i> 5</li>
		        							<li><i class="fa fa-heart" aria-hidden="true"></i> 8</li>
		        						</ul>
		        					</div>
		        				</div> <!-- /.text -->
		        			</div> <!-- /.item -->
		        		</div> <!-- /.course-slider -->
		        	</div>
		        </div>

		        	<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 footer-event" >
	        					<h4>Evenements</h4>
	        					<div class=" row">
	        					<div id="footer-event-carousel" class="carousel slide" data-ride="carousel" style="margin-top:10px;">
								  	  <!-- Indicators -->
									  <ol class="carousel-indicators">
									    <li data-target="#footer-event-carousel" data-slide-to="0" class="active"></li>
									    <li data-target="#footer-event-carousel" data-slide-to="1"></li>
									    <li data-target="#footer-event-carousel" data-slide-to="2"></li>
									  </ol>

									  <!-- Wrapper for slides -->
									  <div class="carousel-inner" role="listbox">
									    <div class="item active">
									      <ul>
									      	<li>
									      		<div class="date p-color-bg">28 <span>Mars</span></div>
									      		<a href="#"><h6>lancement de MAE , conférence de presse</h6></a>
									      		<ul>
					        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
					        						<li><i class="fa fa-tags" aria-hidden="true"></i>ESIH</li>
					        					</ul>
									      	</li>

									      	<li>
									      		<div class="date p-color-bg">Mai <span>2018</span></div>
									      		<a href="event-details.html"><h6>LEARN COUSES ONLINE</h6></a>
									      		<ul>
					        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
					        						<li><i class="fa fa-tags" aria-hidden="true"></i> Gpur Academy</li>
					        					</ul>
									      	</li>

									      	<li>
									      		<div class="date p-color-bg">07 <span>Dec</span></div>
									      		<a href="event-details.html"><h6>COURSES FOR FREE GED</h6></a>
									      		<ul>
					        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
					        						<li><i class="fa fa-tags" aria-hidden="true"></i> Gpur Academy</li>
					        					</ul>
									      	</li>
									      		
									      </ul>
									    </div> <!-- /.item -->

									    <div class="item">
									      <ul>
									      	<li>
									      		<div class="date p-color-bg">27 <span>Dec</span></div>
									      		<a href="event-details.html"><h6>Learning Management</h6></a>
									      		<ul>
					        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
					        						<li><i class="fa fa-tags" aria-hidden="true"></i> Gpur Academy</li>
					        					</ul>
									      	</li>

									      	<li>
									      		<div class="date p-color-bg">19 <span>Dec</span></div>
									      		<a href="event-details.html"><h6>LEARN COUSES ONLINE</h6></a>
									      		<ul>
					        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
					        						<li><i class="fa fa-tags" aria-hidden="true"></i> Gpur Academy</li>
					        					</ul>
									      	</li>

									      	<li>
									      		<div class="date p-color-bg">07 <span>Dec</span></div>
									      		<a href="event-details.html"><h6>COURSES FOR FREE GED</h6></a>
									      		<ul>
					        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
					        						<li><i class="fa fa-tags" aria-hidden="true"></i> Gpur Academy</li>
					        					</ul>
									      	</li>
									      </ul>
									    </div> <!-- /.item -->

									    <div class="item">
									      <ul>
									      	<li>
									      		<div class="date p-color-bg">27 <span>Dec</span></div>
									      		<a href="event-details.html"><h6>Learning Management</h6></a>
									      		<ul>
					        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
					        						<li><i class="fa fa-tags" aria-hidden="true"></i> Gpur Academy</li>
					        					</ul>
									      	</li>

									      	<li>
									      		<div class="date p-color-bg">19 <span>Dec</span></div>
									      		<a href="event-details.html"><h6>LEARN COUSES ONLINE</h6></a>
									      		<ul>
					        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
					        						<li><i class="fa fa-tags" aria-hidden="true"></i> Gpur Academy</li>
					        					</ul>
									      	</li>

									      	<li>
									      		<div class="date p-color-bg">07 <span>Dec</span></div>
									      		<a href="event-details.html"><h6>COURSES FOR FREE GED</h6></a>
									      		<ul>
					        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
					        						<li><i class="fa fa-tags" aria-hidden="true"></i> Gpur Academy</li>
					        					</ul>
									      	</li>
									      </ul>
									    </div> <!-- /.item -->  
									</div>
								</div> <!-- /#footer-event-carousel -->
								</div>
	        				</div> <!-- /.footer-event -->

	        	</div> <!-- /.container -->
	        </div> --}} <!-- /.popular-course -->
	           <div class="event-section wow fadeInUp" style="padding-top: -20px;">

	        	<div class="container">
	        		<div class="row">
	        			<h2 class="text-center" style="margin-bottom: 10px;">Evenements</h2>
	        			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 wow fadeInUp hvr-float-shadow">
	        				<div class="single-event theme-bg-color">
	        					<div class="date p-color">28 <span>Mars 2018</span></div>
	        					<a href="#"><h6>le lancement de MAE</h6></a>
	        					<p>le lancement de MAE , conférence de presse .</p>
	        					<ul>
	        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> ESIH, Ha&iuml;ti</li>
	        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am </li>
	        					</ul>
	        				</div> <!-- /.single-event -->
	        			</div>

	        			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 wow fadeInUp hvr-float-shadow">
	        				<div class="single-event theme-bg-color">
	        					<div class="date p-color">Mai<span> 2018</span></div>
	        					<a href="#"><h6>Ouverture des candidatures</h6></a>
	        					<p>Ouverture des candidatures en ligne  </p>
	        					<ul>
	        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> ESIH, Ha&iuml;ti</li>
	        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am</li>
	        					</ul>
	        				</div> <!-- /.single-event -->
	        			</div>
	        		</div> <!-- /.row -->
	        	</div> <!-- /.container -->
	        </div> <!-- /.event-section -->

	        <!-- Information Banner _______________________ -->
	        <div class="information-banner wow fadeInUp ">
	        	<div class="container">
	        		<h3>Pour plus d'informations sur <span class="p-color">MAE_ESIH</span></h3>
	        		<h6>un diplome d'excellence de niveau international</h6>
	        		<a href="contact-mae-esih" class="p-color-bg tran3s wow fadeInUp">Nous Contacter</a>
	        	</div> <!-- /.container -->
	        </div> <!-- /.information-banner -->
	       
	        <!-- SubsCribe Banner ___________________ -->
	        <div class="subscribe-banner p-color-bg wow fadeInUp info-broadcast">

	        	<div class="container">
	        		<h3 >Inscription a notre info lettre</h3>
	        		<p>Recevez notre info lettre en vous inscrivant</p>
	        		<form action="#" class="clear-fix">
	        			<input type="email" class="float-left wow fadeInLeft" placeholder="votre adresse email">
	        			<button class="float-left tran3s wow fadeInRight">s'inscrire</button>
	        		</form>
	        	</div>
	        </div>
@endsection

