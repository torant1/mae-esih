@extends('layouts.master_home')
@section('content')
			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2 class="text-center">Contactez le MAE_ESIH</h2>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
						<ul class="float-left">
						<li><a href="index">Accueil</a></li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Contactez-nous</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->


			<!-- Contact Us Form _____________________ -->
			<div class="contact-us-page" >
				<div class="container">
					<div class="row">
						<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 wow fadeInLeft">
							<div class="contact-us-form">
								<h3 class="wow fadeInLeft">Ecrivez-nous</h3>
								<p>Votre adresse électronique ne sera ni partagée ni divulguée à d’autres personnes.</p>
								<form action="sendemail" class="form-validation" autocomplete="off" id="form-contact-validation">
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											<input name="name" type="text" placeholder="Nom d’utilisateur">
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											<input name="email" type="email" placeholder="Courriel">
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<input name="sub" type="text" placeholder="Objet">
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<textarea name="message" placeholder="Ecrire un message"></textarea>
											
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<button class="tran3s p-color-bg themehover form-button-envoyer wow fadeInLeft" title="envoyer">Envoyer</button>

											<button type="reset" class="tran3s p-color-bg themehover form-button-effacer wow fadeInRight" id="effacerForm" title="reset">Effacer</button>
											
										</div>
									</div>
								</form>

								<!-- Contact alert -->
								<div class="alert_wrapper" id="alert_success">
									<div id="success">
										<button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
										<div class="wrapper">
							               	<p>Votre message a &eacute;t&eacute; envoy&eacute; avec succes</p>
							             </div>
							        </div>
							    </div> <!-- End of .alert_wrapper -->
							    <div class="alert_wrapper" id="alert_error">
							        <div id="error">
							           	<button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
							           	<div class="wrapper">
							               	<p>Oopps! m&eacute;ssage non envoy&eaute;</p>
							            </div>
							        </div>
							    </div> <!-- End of .alert_wrapper -->
							</div> <!-- /.contact-us-form -->
						</div> <!-- /.col- -->

						<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 wow fadeInRight">
							<div class="contactUs-address">
								<h3 class="wow fadeInRight">CONTACTEZ-NOUS</h3>
			        			<p>Bienvenue sur notre site. N’hésitez surtout pas à nous contacter. </p>

			        			<ul>
	        						<li class="wow fadeInDown">
	        							<i class="fa fa-envelope-o" aria-hidden="true"></i>
	        							<a href="mailto:mae@esih.edu" class="tran3s">mae@esih.edu</a>
	        						</li>
	        						<li>
	        							<i class="fa fa-phone" aria-hidden="true"></i>
	        							<a href="tel:+50922264749" class="tran3s">+509 22 26 47 49</a>
	        						</li>
	        						<li class="wow fadeInUp"><i class="fa fa-map-marker" aria-hidden="true"></i>29, 2e ruelle Nazon, Bourdon, <br> Port-au-Prince, Haïti.</li>
	        					</ul>

							</div> <!-- /.our-address -->
						</div>
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.contact-us-page -->


			<!-- Google Map -->
			<div id="google-map" style="height:460px; width:100%; margin-top:100px;"></div>




			<!-- SubsCribe Banner ___________________ -->
	        <div class="subscribe-banner p-color-bg">
	        	<div class="container">
	        		<h3>Inscription a notre info-lettre</h3>
	        		<p>Recevez notre info-lettre en vous inscrivant</p>
	        		<form action="#" class="clear-fix">
	        			<input type="email" class="float-left wow fadeInLeft" placeholder="votre adresse email">
	        			<button class="float-left tran3s wow fadeInRight">s'inscrire</button>
	        		</form>
	        	</div>
	        </div>


@endsection