@extends('layouts.master_home')
@section('content')

			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2>OUR TEACHER PROFILE</h2>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
					<ul>
						<li><a href="index">Accueil</a></li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Teacher Profile</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->


	        <!-- Teacher Profile Page  ___________________ -->
	        <div class="teacher-profile">
	        	<div class="container">
	        		<div class="row">
	        			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 float-right">
	        				<div class="text-warpper">
	        					<h4>BIOGRAPHY</h4>
	        					<p class="mmfix">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	        						<br> <br>
	        					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	        					</p>
	        					<div class="row">
	        						<div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">
	        							<h4>expertise In Area</h4>
	        							<p>With over 21 years of experience in management, business planning, financial analysis, soft- ware engineering, operations, and decision analysis,</p>
	        							<ul>
	        								<li><i class="fa fa-pencil" aria-hidden="true"></i>Growth strategy</li>
	        								<li><i class="fa fa-pencil" aria-hidden="true"></i>21 years of experience in entrepreneurship</li>
	        								<li><i class="fa fa-pencil" aria-hidden="true"></i>M&A transaction support</li>
	        								<li><i class="fa fa-pencil" aria-hidden="true"></i>Significant and related to teaching</li>
	        								<li><i class="fa fa-pencil" aria-hidden="true"></i>Completed in schools or other community</li>
	        								<li><i class="fa fa-pencil" aria-hidden="true"></i>Instructional in nature</li>
	        								<li><i class="fa fa-pencil" aria-hidden="true"></i>Significant and related to teaching</li>
	        								<li><i class="fa fa-pencil" aria-hidden="true"></i>MBA, Rotterdam School of Management</li>
	        								<li><i class="fa fa-pencil" aria-hidden="true"></i>BS, engineering, Technical University of Dhaka</li>
	        							</ul>
	        						</div> <!-- /.col- -->

	        						<div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">
	        							<h4>Extra Cariculam</h4>
	        							<div class="teachers-bio-panel">
											<div class="panel-group theme-accordion" id="accordion">
											  <div class="panel">
											    <div class="panel-heading active-panel">
											      <h6 class="panel-title">
											        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
											        Growth strategy</a>
											      </h6>
											    </div>
											    <div id="collapse1" class="panel-collapse collapse in">
											      <div class="panel-body">
											      	<p>Primary/Junior (B10) and Junior/Intermediate (B20) applicants should have some experience working with children and youth of the age group in the program </p>
											      </div>
											    </div>
											  </div> <!-- /panel 1 -->
											  <div class="panel">
											    <div class="panel-heading">
											      <h6 class="panel-title">
											        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
											         Business transformation</a>
											      </h6>
											    </div>
											    <div id="collapse2" class="panel-collapse collapse">
											      <div class="panel-body">
											      	<p>Primary/Junior (B10) and Junior/Intermediate (B20) applicants should have some experience working with children and youth of the age group in the program </p>
											      </div>
											    </div>
											  </div> <!-- /panel 2 -->
											  <div class="panel">
											    <div class="panel-heading">
											      <h6 class="panel-title">
											        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
											        Leadership position</a>
											      </h6>
											    </div>
											    <div id="collapse3" class="panel-collapse collapse">
											      <div class="panel-body">
											      	<p>Primary/Junior (B10) and Junior/Intermediate (B20) applicants should have some experience working with children and youth of the age group in the program </p>
											      </div>
											    </div>
											  </div> <!-- /panel 3 -->

											  <div class="panel">
											    <div class="panel-heading">
											      <h6 class="panel-title">
											        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
											        Instructional in nature</a>
											      </h6>
											    </div>
											    <div id="collapse4" class="panel-collapse collapse">
											      <div class="panel-body">
											      	<p>Primary/Junior (B10) and Junior/Intermediate (B20) applicants should have some experience working with children and youth of the age group in the program </p>
											      </div>
											    </div>
											  </div> <!-- /panel 4 -->

											</div> <!-- end #accordion -->
										</div> <!-- End of .teachers-bio-panel -->
	        						</div>
	        					</div>
	        				</div> <!-- /.text-wrapper -->
	        			</div>

	        			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 teacher-sidebar float-left wow fadeInUp">
	        				<div class="img-holder theme-bg-color"><img src="images/inner-page/3.jpg" alt="Teacher"></div>
	        				<div class="teachers-bio p-color-bg">
	        					<h6>Yves Bah</h6>
	        					<p><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Science and Technology</p>
	        					<p><i class="fa fa-graduation-cap" aria-hidden="true"></i>Technology Specialties</p>
	        					<p><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="#">yves.bah@monoingroup.com</a></p>
	        					<p><i class="fa fa-phone" aria-hidden="true"></i> <a href="#">+8801712570051</a></p>
	        					<p><i class="fa fa-location-arrow" aria-hidden="true"></i> <a href="#">monoigroup.com</a></p>

	        					<ul>
									<li><a href="#" class="tran3s round-border icon"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			        				<li><a href="#" class="tran3s round-border icon"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a href="#" class="tran3s round-border icon"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
									<li><a href="#" class="tran3s round-border icon"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
								</ul>
	        				</div> <!-- /.teachers-bio -->
	        			</div> <!-- /.teacher-sidebar -->
	        		</div> <!-- /.row -->
	        	</div> <!-- /.container -->
	        </div> <!-- /.teacher-profile -->
	        


	        <!-- Our Teacher ________________________ -->
	        <div class="our-teacher wow fadeInUp" style="margin-bottom:10em;">
	        	<div class="container">
	        		<h4>Autres Professeurs</h4>

	        		<div class="row">
	        			<div class="theme-slider">
	        				<div class="item">
	        					<div class="item-wrapper theme-bg-color tran3s hvr-float-shadow">
	        						<div class="img-holder round-border">
		        						<img src="images/inner-page/2.jpg" alt="Teacher">
		        						<div class="overlay round-border tran4s">
		        							<ul>
												<li><a href="#" class="tran3s round-border icon"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						        				<li><a href="#" class="tran3s round-border icon"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
												<li><a href="#" class="tran3s round-border icon"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
											</ul>
		        						</div>
		        					</div> <!-- /.img-holder -->
		        					<a href="#"><h6>Jaflika tesla</h6></a>
		        					<p>Creative Director</p>
	        					</div>
	        				</div> <!-- /.item -->

	        				<div class="item">
	        					<div class="item-wrapper theme-bg-color tran3s hvr-float-shadow">
	        						<div class="img-holder round-border">
		        						<img src="images/inner-page/3.jpg" alt="Teacher">
		        						<div class="overlay round-border tran4s">
		        							<ul>
												<li><a href="#" class="tran3s round-border icon"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						        				<li><a href="#" class="tran3s round-border icon"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
												<li><a href="#" class="tran3s round-border icon"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
											</ul>
		        						</div>
		        					</div> <!-- /.img-holder -->
		        					<a href="#"><h6>Mahfuz Riad</h6></a>
		        					<p>Science Professor</p>
	        					</div>
	        				</div> <!-- /.item -->

	        				<div class="item">
	        					<div class="item-wrapper theme-bg-color tran3s hvr-float-shadow">
	        						<div class="img-holder round-border">
		        						<img src="images/inner-page/4.jpg" alt="Teacher">
		        						<div class="overlay round-border tran4s">
		        							<ul>
												<li><a href="#" class="tran3s round-border icon"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						        				<li><a href="#" class="tran3s round-border icon"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
												<li><a href="#" class="tran3s round-border icon"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
											</ul>
		        						</div>
		        					</div> <!-- /.img-holder -->
		        					<a href="#"><h6>Rashed jannat</h6></a>
		        					<p>lecturer</p>
	        					</div>
	        				</div> <!-- /.item -->

	        				<div class="item">
	        					<div class="item-wrapper theme-bg-color tran3s hvr-float-shadow">
	        						<div class="img-holder round-border">
		        						<img src="images/inner-page/2.jpg" alt="Teacher">
		        						<div class="overlay round-border tran4s">
		        							<ul>
												<li><a href="#" class="tran3s round-border icon"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						        				<li><a href="#" class="tran3s round-border icon"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
												<li><a href="#" class="tran3s round-border icon"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
											</ul>
		        						</div>
		        					</div> <!-- /.img-holder -->
		        					<a href="#"><h6>Jaflika tesla</h6></a>
		        					<p>Creative Director</p>
	        					</div>
	        				</div> <!-- /.item -->

	        				<div class="item">
	        					<div class="item-wrapper theme-bg-color tran3s hvr-float-shadow">
	        						<div class="img-holder round-border">
		        						<img src="images/inner-page/3.jpg" alt="Teacher">
		        						<div class="overlay round-border tran4s">
		        							<ul>
												<li><a href="#" class="tran3s round-border icon"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						        				<li><a href="#" class="tran3s round-border icon"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
												<li><a href="#" class="tran3s round-border icon"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
											</ul>
		        						</div>
		        					</div> <!-- /.img-holder -->
		        					<a href="#"><h6>Mahfuz Riad</h6></a>
		        					<p>Science Professor</p>
	        					</div>
	        				</div> <!-- /.item -->

	        				<div class="item">
	        					<div class="item-wrapper theme-bg-color tran3s hvr-float-shadow">
	        						<div class="img-holder round-border">
		        						<img src="images/inner-page/4.jpg" alt="Teacher">
		        						<div class="overlay round-border tran4s">
		        							<ul>
												<li><a href="#" class="tran3s round-border icon"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						        				<li><a href="#" class="tran3s round-border icon"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
												<li><a href="#" class="tran3s round-border icon"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
											</ul>
		        						</div>
		        					</div> <!-- /.img-holder -->
		        					<a href="#"><h6>Rashed jannat</h6></a>
		        					<p>lecturer</p>
	        					</div>
	        				</div> <!-- /.item -->

	        				<div class="item">
	        					<div class="item-wrapper theme-bg-color tran3s hvr-float-shadow">
	        						<div class="img-holder round-border">
		        						<img src="images/inner-page/2.jpg" alt="Teacher">
		        						<div class="overlay round-border tran4s">
		        							<ul>
												<li><a href="#" class="tran3s round-border icon"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						        				<li><a href="#" class="tran3s round-border icon"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
												<li><a href="#" class="tran3s round-border icon"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
											</ul>
		        						</div>
		        					</div> <!-- /.img-holder -->
		        					<a href="#"><h6>Jaflika tesla</h6></a>
		        					<p>Creative Director</p>
	        					</div>
	        				</div> <!-- /.item -->
	        				
	        			</div>
	        		</div> <!-- /.row -->
	        	</div> <!-- /.container -->
	        </div> <!-- /.our-teacher -->

@endsection