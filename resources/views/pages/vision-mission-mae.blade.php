@extends('layouts.master_home')
@section('content')
			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2 class="text-center">Vision - Mission</h2>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
					<ul>
						<li><a href="index">Accueil</a></li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>A propos</li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Mission et Vision</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->

			<!-- Event Section _______________________ -->
	        <div class="vision-section wow fadeInDown" data-wow-offset="300">
	        	<div class="container">
					<div class="row">
						<div class="text-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<h2 class=" text-bold text-title  wow fadeInDown" data-wow-offset="300" >Vision du MAE_ESIH</h2>
							<p>
								La vision du <span class="text-red">MASTER EN ADMINISTRATION DES ENTREPRISES</span> Spécialité <span class="text-red">DIRECTION DES
								ENTREPRISES</span> est de mettre à la portée des professionnel(le)s et meilleur(e)s étudiant(e)s
								haïtien(ne)s, un programme européen moderne de haut niveau en création et gestion des
								entreprises aux fins de favoriser l’émergence d’ une nouvelle génération de managers
								haïtiens dans un contexte de création de valeur par l’acquisition des compétences du 21ème
								siècle.
							</p>
								
						</div>
					    			
					</div>    		
	        	</div>
	        </div>
	        <div class="mission-section">
	        	<div class="container">
					<div class="row">
						<div class="text-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<h2 class="text-bold text-title wow fadeInUp">Mission du MAE_ESIH</h2>
							<p>
								La mission du programme <span class="text-red">MASTER EN ADMINISTRATION DES ENTREPRISES</span> Spécialité <span class="text-red">DIRECTION DES
								ENTREPRISES</span> est de mettre à la disposition des entreprises et institutions
									haïtiennes des managers possédant une approche globale de l’organisation. Les cadres en
									exercice et futurs managers seront formés à la mise en oeuvre pratique, adaptée et
									évolutive des connaissances acquises pour faire face aux multiples défis auxquels
									Entreprises et Institutions sont (et seront) confrontés.
							</p>
							<br>
							<p>
								Dans une économie technologique globalisée et hyperconcurrentielle, le 
								MASTER MAE-DE se positionne comme un pôle d’excellence de formation en management
								en Haïti et dans la Caraïbe.
								</p>	
								<br>
								<p>
								Il s’agit à travers la délivrance de ce diplôme international, de proposer un cursus alliant
								savoir-faire, innovation managériale, intelligence technologique et intégration des contextes
								nationaux et régionaux.
							</p>	
								
						</div>
					    			
					</div>    		
	        	</div>
	        </div>
	
	  
@endsection