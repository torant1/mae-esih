@extends('layouts.master_home')
@section('content')
			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2 class="text-center text-uppercase text-white">Medias sociaux et galerie</h2>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
					<ul>
						<li><a href="index">Accueil</a></li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Actualit&eacute;s</li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Media Galleries</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->
 
 {{-- section Link to our youtube Channel --}}
			<!-- Event Section _______________________ -->
	        <div class="event-details-page">
	        	<div class="container">
	        		<div class="row">
	        			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
	        				<div class="wraper">
	        					{{-- Div Link to youtube video section --}}
        					<div class="sidebar-box quick-event-list">

	        						<div class="video-wrapper" >
	        							<div class="video-container" style="height: 50%;">
	        							<iframe src="https://www.youtube.com/embed/ScyXY1mg8GQ?autoplay=1&rel=0" allowfullscreen="allowfullscreen" style="width: 100%; height: 25em; " frameborder="0" ></iframe>

	        							</div>
	        						</div>
	        				</div>
	        				</div>
	        				
	        			</div>
				<!-- _________________ SideBar _________________ -->
	        			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 sidebarOne float-left">
	        				<div class="wrapper">
	        					<div class="sidebar-box our-brochure">
	        						<div class="box-wrapper">
	        							<h4>Follow us </h4>
	        							<a href="" class="tran3s"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</a>
	        							<a href="" class="tran3s"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a>
	        							<a href="" class="tran3s"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a>
	        							<a href="" class="tran3s"><i class="fa fa-youtube" aria-hidden="true"></i> youtube</a>
	        							<a href="" class="tran3s"><i class="fa fa-rss" aria-hidden="true"></i> rss</a>
	        							
	        						</div> <!-- /.box-wrapper -->
	        					</div> <!-- /.sidebar-box.our-brochure -->
	        				</div> <!-- /.wrapper -->
	        			</div> <!-- /.sidebarOne -->
	        			<!-- ___________ CHannel Youtube _____________-->
	        			<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
		        	 		<div class="video-youtube-content col-lg-3 col-md-3">
		        	 				<div class="video-wrapper" >
		        							<div class="video-container" style="height: 100%;">
		        							<iframe src="https://www.youtube-nocookie.com/embed/XdyLp0_-r0s?rel=0" allowfullscreen="allowfullscreen" style="width: 100%; " frameborder="0" ></iframe>

		        							</div>
		        							<div class="text-content youtube-video-content">
		        								<span class="youtube-video-title">
 													<?php 
 		$json=json_decode(file_get_contents('http://www.youtube.com/oembed?url=http%3A//youtube.com/watch?v=XdyLp0_-r0s&format=json'));
 														echo($json->title);
 													?>
		        								</span>
		        							</div>
		        					</div>
		        	 		</div>
		        	 		<div class=" video-youtube-content col-lg-3 col-md-3">
		        	 				<div class="video-wrapper" >
		        							<div class="video-container" style="height: 50%;">
		        							<iframe src="https://www.youtube-nocookie.com/embed/TZWIrai9TFc?rel=0" allowfullscreen="allowfullscreen" style="width: 100%;  " frameborder="0" ></iframe>

		        							</div>
		        					</div>
		        					<div class="text-content youtube-video-content">
		        								<span class="youtube-video-title">
 													<?php 
 		$json=json_decode(file_get_contents('http://www.youtube.com/oembed?url=http%3A//youtube.com/watch?v=TZWIrai9TFc&format=json'));
 														echo($json->title);
 													?>
		        								</span>
		        							</div>
		        	 		</div>
		        	 		<div class="video-youtube-content col-lg-3 col-md-3">
		        	 				<div class="video-wrapper" >
		        							<div class="video-container" style="height: 50%;">
		        							<iframe src="https://www.youtube-nocookie.com/embed/gFnEwbd9Zd0?rel=0" allowfullscreen="allowfullscreen" style="width: 100%;" frameborder="0" ></iframe>

		        							</div>
		        					</div>
		        					<div class="text-content youtube-video-content">
		        								<span class="youtube-video-title">
 													<?php 
 		$json=json_decode(file_get_contents('http://www.youtube.com/oembed?url=http%3A//youtube.com/watch?v=gFnEwbd9Zd0&format=json'));
 														echo($json->title);
 													?>
		        								</span>
		        					</div>
		        	 		</div>
		        	 		<div class="video-youtube-content col-lg-3 col-md-3">
		        	 				<div class="video-wrapper" >
		        							<div class="video-container" style="height: 50%;">
		        							<iframe src="https://www.youtube-nocookie.com/embed/bayBzIhesqY?rel=0" allowfullscreen="allowfullscreen" style="width: 100%; " frameborder="0" ></iframe>

		        							</div>
		        					</div>
		        					<div class="text-content youtube-video-content">
		        								<span class="youtube-video-title">
 													<?php 
 		$json=json_decode(file_get_contents('http://www.youtube.com/oembed?url=http%3A//youtube.com/watch?v=bayBzIhesqY&format=json'));
 														echo($json->title);
 													?>
		        								</span>
		        					</div>
		        	 		</div>

	        	 	
	        	 		</div>
	        	 		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
		        	 		<div class="video-youtube-content col-lg-3 col-md-3">
		        	 				<div class="video-wrapper" >
		        							<div class="video-container" style="height: 100%;">
		        							<iframe src="https://www.youtube.com/embed/Qlesfif9kbE?rel=0" allowfullscreen="allowfullscreen" style="width: 100%; " frameborder="0" ></iframe>

		        							</div>
		        					</div>
		        					<div class="text-content youtube-video-content">
		        								<span class="youtube-video-title">
 													<?php 
 		$json=json_decode(file_get_contents('http://www.youtube.com/oembed?url=http%3A//youtube.com/watch?v=Qlesfif9kbE&format=json'));
 														echo($json->title);
 													?>
		        								</span>
		        					</div>
		        	 		</div>
		        	 		<div class="video-youtube-content col-lg-3 col-md-3">
{{-- 		        	 				<div class="video-wrapper" >
		        							<div class="video-container" style="height: 50%;">
		        							<iframe src="https://www.youtube-nocookie.com/embed/u1H0FSuPWxs?rel=0" allowfullscreen="allowfullscreen" style="width: 100%;  " frameborder="0" ></iframe>

		        							</div>
		        					</div> --}}
		        				
		        	 		</div>
		        	 	
	        	 	
	        	 		</div>
	        		</div>
	        	</div>
	        </div>
	        {{-- section galery --}}
	     {{--    <div>
	        	<div class="container">
	        	 <div class="row">
	        	 	<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
	        	 		<div class="col-lg-4 col-md-4">
	        	 				<div class="video-wrapper" >
	        							<div class="video-container" style="height: 50%;">
	        							<iframe src="https://www.youtube-nocookie.com/embed/XdyLp0_-r0s?autoplay=1" allowfullscreen="allowfullscreen" style="width: 100%; height: 25em; " frameborder="0" ></iframe>

	        							</div>
	        					</div>
	        	 		</div>
	        	 		<div class="col-lg-4 col-md-4">
	        	 				<div class="video-wrapper" >
	        							<div class="video-container" style="height: 50%;">
	        							<iframe src="https://www.youtube-nocookie.com/embed/XdyLp0_-r0s?autoplay=1" allowfullscreen="allowfullscreen" style="width: 100%; height: 25em; " frameborder="0" ></iframe>

	        							</div>
	        					</div>
	        	 		</div>
	        	 		<div class="col-lg-4 col-md-4">
	        	 				<div class="video-wrapper" >
	        							<div class="video-container" style="height: 50%;">
	        							<iframe src="https://www.youtube-nocookie.com/embed/XdyLp0_-r0s?autoplay=1" allowfullscreen="allowfullscreen" style="width: 100%; height: 25em; " frameborder="0" ></iframe>

	        							</div>
	        					</div>
	        	 		</div>
	        	 		<div class="col-lg-4 col-md-4">
	        	 				<div class="video-wrapper" >
	        							<div class="video-container" style="height: 50%;">
	        							<iframe src="https://www.youtube-nocookie.com/embed/XdyLp0_-r0s?autoplay=1" allowfullscreen="allowfullscreen" style="width: 100%; height: 25em; " frameborder="0" ></iframe>

	        							</div>
	        					</div>
	        	 		</div>
	        	 	
	        	 	</div>
	        	 </div>
	        		
	        	</div>
	        	
	        </div> --}}
	       	<!-- SubsCribe Banner ___________________ -->
	        <div class="subscribe-banner p-color-bg wow fadeInUp">
	        	<div class="container">
	        		<h3>Inscription a notre info lettre</h3>
	        		<p>Recevez notre info lettre en vous inscrivant</p>
	        		<form action="#" class="clear-fix">
	        			<input type="email" class="float-left wow fadeInLeft" placeholder="votre adresse email">
	        			<button class="float-left tran3s wow fadeInRight">S'inscrire</button>
	        		</form>
	        	</div>
	        </div>
@endsection

