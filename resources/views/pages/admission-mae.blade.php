@extends('layouts.master_home')
@section('content')
			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2 class="text-center">Conditions d'admission</h2>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
					<ul>
						<li><a href="index">Accueil</a></li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Admission</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->

			<div class="admission-section">
				        	<div class="container">
								<div class="row">
									<div class="text-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<img src="https://i.pinimg.com/originals/96/22/54/9622549b1199b7c85e944d82847db430.png" alt="image processus admision ESIH">
									</div>
								</div>
							</div>
			</div>
			<!-- Event Section _______________________ -->
	        <div class="gouvernace-section">
	        	<div class="container">
					<div class="row">
						<div class="text-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<h2 class=" text-bold text-title">Processus de candidature</h2>
							<br>
							<br>
		<h3 class="text-red">Candidature</h3>
		<br>
							<p>
								Notre processus de dépôt de candidature est entièrement dématérialisé et s'effectue exclusivement en ligne sur nos sites dédiés (NICE  et PORT-AU-PRINCE)
							</p>
								<br>
								<p>
									 Lors du dépôt de votre candidature, nous vous invitons à :
								<ul>
								    <li><span class=" text-list">Compléter votre profil ;</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="1s"><span class=" text-list"> Choisir vos formations ;</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="2s"><span class=" text-list">Envoyer les documents pour compléter votre dossier. </span></li>
								</ul>
							</p>
							<br>
							<br>
							<h3 class="text-red">Admissibilit&eacute;</h3>
								<br>
							<p>
								<ul>
								    <li><span class=" text-list">Les dossiers sont examinés par le Comité de sélection binational (IAE-Nice et ESIH).</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="1s"><span class=" text-list"> Le paiement de frais d’examen de dossier : 7500 Gourdes</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="2s"><span class=" text-list"> Les candidats présélectionnés sur dossier comme étant "admissibles" sont invités par e-mail à un entretien individuel avec le Jury.</span></li>			
								</ul>

							</p>
							<br>
							<br>
							<h3 class="text-red">Entretien</h3>
								<br>
							<p>
								<ul>
								    <li><span class=" text-list">  Les entretiens d'admission avec les membres du Comité de sélection constitué en Jury ont lieu dans les locaux du MAE_ESIH.</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="1s"><span class=" text-list">Votre convocation par courrier électronique indique la date et l'heure exactes d'entretien du candidat ou de la candidate.</span></li>		
								</ul>

							</p>
							<br>
								<p>
									<h4 class="text-center text-bold text-title" style="text-decoration: underline;">La présence à l’entretien est obligatoire.</h4>
								</p>
							<p>
								
								L'entretien peut exceptionnellement se dérouler par vidéo-conférence, avec l’accord express du Jury. Les modalités et conditions pour la demande d'entretien à distance seront indiquées dans le message électronique de convocation. A noter que les entretiens téléphoniques (sans contact vidéo) sont strictement interdits, et de ce fait, exclus.
								<br>
								<br>
								Lors de cet entretien vous serez amené (e) à exposer vos motivations, votre plan de carrière, ainsi que la maîtrise que vous avez du langage technique et managérial de l’entreprise. 
							</p>
							<br>
							<br>
								<h3 class="text-red">Admission</h3>
								<br>
							<p class="wow fadeInUp" >
								Les candidats retenus à la suite de l’entretien sont déclarés "admis" et reçoivent un courrier électronique. Les candidats déclarés "admis" doivent confirmer leur admission dans un délai de 10 jours.
							</p>

								<br>
								<p class="wow fadeInUp" >
								<span class="text-red">Nota Bene :</span> Veuillez noter que les places disponibles pour chaque formation sont limitées (30). Les candidats admis sur liste d'attente ne seront contactés qu’en cas de désistement d'un candidat admis. 
								</p>
								<br>
								<p class="wow fadeInUp" >
									<span class="text-bold" style="color: red; text-decoration: underline;">Attention :</span> Le document original est envoyé par défaut à l'adresse fournie lors de la candidature (celle qui apparaît dans votre "profil"). Si vous souhaitez que toute correspondance avec vous soit envoyée à une adresse différente, veillez à le spécifier dans la rubrique demande d'envoi.
								</p>
							<br>
							<br>
							<h3 class="text-red">Confirmation</h3>
								<br>
								<p class="wow fadeInUp">
									Si vous êtes admis, vous devez impérativement confirmer votre souhait d'être inscrit(e) dans un délai de 10 jours. Passé ce délai, votre place est irrévocablement proposée aux candidat(e)s sur liste d'attente.
									<br>
									La confirmation se fait exclusivement en ligne via votre profil, en suivant les instructions contenues dans le mail d'admission.

								</p>
								<br>
								<p class="wow fadeInUp">
									Une fois que vous aurez confirmé, vous recevrez un message vous transmettant les informations qui vous permettrons de parachever votre inscription. Vous pourrez être amené(e) à produire des pièces complémentaires et, le cas échéant, un justificatif de paiement de vos droits de scolarité.
								</p>
								<br>
								<p class="wow fadeInUp">
									<span class="text-bold" style="color: red; text-decoration: underline;">Attention :</span> Votre place n’est sécurisée qu’à partir du moment où l’Economat reçoit votre confirmation/inscription et preuve du paiement de tout ou partie de vos frais scolaires en fonction des modalités de paiement arrêtées pour le MAE_ESIH. En cas de non-respect des délais et échéances de la part d’un candidat admis, un(e) candidat(e) sur liste d'attente se verra offrir l’opportunité de prendre sa place.
								</p>
						</div>
					    			
					</div>    		
	        	</div>
	        </div>
@endsection