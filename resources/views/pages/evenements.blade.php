@extends('layouts.master_home')
@section('content')
			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2 class="text-center">EVENEMENTS</h2>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
					<ul>
						<li><a href="index">Accueil</a></li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Actualit&eacute;s</li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Ev&egrave;nements</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->

			<!-- Event Section _______________________ -->
	        <div class="event-section wow fadeInUp">
	        	<div class="container">
	        		<div class="row">
	        			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp hvr-float-shadow">
	        				<div class="single-event theme-bg-color">
	        					<div class="date p-color">25 <span>June</span></div>
	        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
	        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
	        					<ul>
	        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
	        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
	        					</ul>
	        				</div> <!-- /.single-event -->
	        			</div>

	        			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp hvr-float-shadow">
	        				<div class="single-event theme-bg-color">
	        					<div class="date p-color">26 <span>June</span></div>
	        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
	        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
	        					<ul>
	        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
	        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
	        					</ul>
	        				</div> <!-- /.single-event -->
	        			</div>

	        			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp hvr-float-shadow">
	        				<div class="single-event theme-bg-color">
	        					<div class="date p-color">27 <span>June</span></div>
	        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
	        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
	        					<ul>
	        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
	        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
	        					</ul>
	        				</div> <!-- /.single-event -->
	        			</div>

	        			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp hvr-float-shadow">
	        				<div class="single-event theme-bg-color">
	        					<div class="date p-color">28 <span>June</span></div>
	        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
	        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
	        					<ul>
	        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
	        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
	        					</ul>
	        				</div> <!-- /.single-event -->
	        			</div>

	        			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp hvr-float-shadow">
	        				<div class="single-event theme-bg-color">
	        					<div class="date p-color">29 <span>June</span></div>
	        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
	        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
	        					<ul>
	        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
	        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
	        					</ul>
	        				</div> <!-- /.single-event -->
	        			</div>

	        			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp hvr-float-shadow">
	        				<div class="single-event theme-bg-color">
	        					<div class="date p-color">30 <span>June</span></div>
	        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
	        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
	        					<ul>
	        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
	        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
	        					</ul>
	        				</div> <!-- /.single-event -->
	        			</div>

	        			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp hvr-float-shadow">
	        				<div class="single-event theme-bg-color">
	        					<div class="date p-color">25 <span>June</span></div>
	        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
	        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
	        					<ul>
	        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
	        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
	        					</ul>
	        				</div> <!-- /.single-event -->
	        			</div>

	        			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp hvr-float-shadow">
	        				<div class="single-event theme-bg-color">
	        					<div class="date p-color">26 <span>June</span></div>
	        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
	        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
	        					<ul>
	        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
	        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
	        					</ul>
	        				</div> <!-- /.single-event -->
	        			</div>

	        			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp hvr-float-shadow">
	        				<div class="single-event theme-bg-color">
	        					<div class="date p-color">27 <span>June</span></div>
	        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
	        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
	        					<ul>
	        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
	        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
	        					</ul>
	        				</div> <!-- /.single-event -->
	        			</div>

	        			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp hvr-float-shadow">
	        				<div class="single-event theme-bg-color">
	        					<div class="date p-color">28 <span>June</span></div>
	        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
	        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
	        					<ul>
	        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
	        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
	        					</ul>
	        				</div> <!-- /.single-event -->
	        			</div>

	        			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp hvr-float-shadow">
	        				<div class="single-event theme-bg-color">
	        					<div class="date p-color">29 <span>June</span></div>
	        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
	        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
	        					<ul>
	        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
	        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
	        					</ul>
	        				</div> <!-- /.single-event -->
	        			</div>

	        			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp hvr-float-shadow">
	        				<div class="single-event theme-bg-color">
	        					<div class="date p-color">30 <span>June</span></div>
	        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
	        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
	        					<ul>
	        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
	        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
	        					</ul>
	        				</div> <!-- /.single-event -->
	        			</div>
	        		</div> <!-- /.row -->
	        	</div> <!-- /.container -->
	        </div> <!-- /.event-section -->


	        <!-- Latest Event Slider Section _______ -->
	        <div class="latest-event-slider event-section wow fadeInUp">
	        	<div class="container">
	        		<h3>free Important Events</h3>

	        		<div class="row">
	        			<div class="theme-slider">
	        				<div class="item hvr-float-shadow">
	        					<img src="{{asset('images/inner-page/9.jpg')}}" alt="Image">
		        				<div class="single-event theme-bg-color">
		        					<div class="date p-color">25 <span>June</span></div>
		        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
		        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
		        					<ul>
		        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
		        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
		        					</ul>
		        				</div> <!-- /.single-event -->
		        			</div> <!-- /.item -->

		        			<div class="item hvr-float-shadow">
		        				<img src="{{asset('images/inner-page/10.jpg')}}" alt="Image">
		        				<div class="single-event theme-bg-color">
		        					<div class="date p-color">30 <span>Dec</span></div>
		        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
		        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
		        					<ul>
		        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
		        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
		        					</ul>
		        				</div> <!-- /.single-event -->
		        			</div> <!-- /.item -->

		        			<div class="item hvr-float-shadow">
		        				<img src="{{asset('images/inner-page/11.jpg')}}" alt="Image">
		        				<div class="single-event theme-bg-color">
		        					<div class="date p-color">15 <span>June</span></div>
		        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
		        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
		        					<ul>
		        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
		        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
		        					</ul>
		        				</div> <!-- /.single-event -->
		        			</div> <!-- /.item -->

		        			<div class="item hvr-float-shadow">
		        				<img src="{{asset('images/inner-page/9.jpg')}}" alt="Image">
		        				<div class="single-event theme-bg-color">
		        					<div class="date p-color">05 <span>Sep</span></div>
		        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
		        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
		        					<ul>
		        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
		        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
		        					</ul>
		        				</div> <!-- /.single-event -->
		        			</div> <!-- /.item -->

		        			<div class="item hvr-float-shadow">
		        				<img src="{{asset('images/inner-page/10.jpg')}}" alt="Image">
		        				<div class="single-event theme-bg-color">
		        					<div class="date p-color">11 <span>June</span></div>
		        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
		        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
		        					<ul>
		        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
		        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
		        					</ul>
		        				</div> <!-- /.single-event -->
		        			</div> <!-- /.item -->

		        			<div class="item hvr-float-shadow">
		        				<img src="{{asset('images/inner-page/11.jpg')}}" alt="Image">
		        				<div class="single-event theme-bg-color">
		        					<div class="date p-color">20 <span>Nov</span></div>
		        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
		        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
		        					<ul>
		        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
		        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
		        					</ul>
		        				</div> <!-- /.single-event -->
		        			</div> <!-- /.item -->

		        			<div class="item hvr-float-shadow">
		        				<img src="{{asset('images/inner-page/9.jpg')}}" alt="Image">
		        				<div class="single-event theme-bg-color">
		        					<div class="date p-color">10 <span>May</span></div>
		        					<a href="event-details"><h6>Build Education STRATEGY</h6></a>
		        					<p>Tech you how to build a complete Learning Management Offering a various of solutions meeting your need no matter</p>
		        					<ul>
		        						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Barisal,CA</li>
		        						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10am - 05pm</li>
		        					</ul>
		        				</div> <!-- /.single-event -->
		        			</div> <!-- /.item -->
	        			</div> <!-- /.theme-slider -->
	        		</div> <!-- /.row -->
	        	</div> <!-- /.container -->
	        </div> <!-- /.latest-event-slider -->




			<!-- SubsCribe Banner ___________________ -->
	        <div class="subscribe-banner p-color-bg wow fadeInUp">
	        	<div class="container">
	        		<h3>Inscription a notre info lettre</h3>
	        		<p>Recevez notre info lettre en vous inscrivant</p>
	        		<form action="#" class="clear-fix">
	        			<input type="email" class="float-left wow fadeInLeft" placeholder="votre adresse email">
	        			<button class="float-left tran3s wow fadeInRight">s'inscire</button>
	        		</form>
	        	</div>
	        </div>

@endsection