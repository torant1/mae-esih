@extends('layouts.master_home')
@section('content')
			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2>Optimisez  votre communication et la diffusion de vos offres</h2>
						<h4 class="text-inner-second">une vitrine unique ou placer vos offres de recruitement</h4>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
					<ul class="float-left">
						<li><a href="index">Accueil</a></li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Espace  Carri&egrave;res</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->

			<!-- Event Section _______________________ -->
	        <div class="partenaire-section">
	        	<div class="container">
					<div class="row">
						<div class="text-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<h2 class=" text-bold text-title">Le MAE-DE, un vivier pour recruter vos futurs collaborateurs</h2>
								<p>
								Acteurs socio-économiques, vous avez tout à gagner à développer des passerelles de
								collaboration avec
								Le MAE-DE de l’IAE de NICE et de l’ESIH.
								Venez rencontrer les forces vives de demain, ceux et celles qui
								apporteront du sang neuf à votre entreprise  ?
						</p>
						<br>
						<p>
								Le <span class="text-red">MAE-DE de l’IAE de NICE</span> et de <span class="text-red">l’ESIH</span> vous propose des outils de collaboration qui vous permettent
								de valoriser votre marque employeur et d'anticiper vos besoins en cadres supérieurs et en ressources
								humaines de grande qualité. Pourquoi ne pas profiter des temps forts du <span class="text-red">MAE-DE</span>  et des
								forums pour organiser une présentation ou planifier vos sessions de recrutement ?
							</p>
							<br>
							<p class="list-offre">
								<span class="offre-stage"> <a href="#">Voir offres de stage<i class="fa-arrow-link fa fa-angle-double-right"></i></a></span>	
								<span class="offre-emploi"> <a href="#">Voir offres d'emploi <i class="fa-arrow-link fa fa-angle-double-right"></i></a></span>	
							</p>
								
						</div>
					    			
					</div>    		
	        	</div>
	        </div>
	  
@endsection