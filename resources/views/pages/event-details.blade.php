@extends('layouts.master_home')
@section('content')
			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2>Events SINGLE</h2>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
					<ul>
						<li><a href="index.html">Accueil</a></li>
						<li><i class="fa fa-caret-right"></i></li>
						<li> Details Evenement</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->



			<!-- Event Section _______________________ -->
	        <div class="event-details-page">
	        	<div class="container">
	        		<div class="row">
	        			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 float-right">
	        				<div class="event-details-content clear-fix">
	        					<img src="images/inner-page/12.jpg" alt="Image">
	        					<h3>economic text development courses</h3>

	        					<div class="sub-text">
	        						<h4>EVENT DESCRIPTION</h4>
	        						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
	        					</div> <!-- /.sub-text -->
	        					<div class="sub-text">
	        						<h4>EVENT OUTPUT</h4>
	        						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled </p>
	        						<ul class="event-output-list">
	        							<li><i class="fa fa-graduation-cap" aria-hidden="true"></i> A delightful way to teach kids about computers</li>
	        							<li><i class="fa fa-graduation-cap" aria-hidden="true"></i> Our campaign to ban plastic bags in Bali</li>
	        							<li><i class="fa fa-graduation-cap" aria-hidden="true"></i> Over 43 lectures and 60 hours of content </li>
	        						</ul>
	        					</div> <!-- /.sub-text -->
	        					<div class="sub-text">
	        						<h4>Location/map</h4>
	        						<p><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;&nbsp;Diet and health, human osteology,army studim ,CA 12342</p>
	        						<div class="clear-fix">
	        							<!-- Google Map -->
										<div id="google-map" class="float-left wow fadeInLefr"></div>

										<div class="float-right wow fadeInRight table-fix">
											<div class="table-wrapper">
												<table>
													<tr>
														<td><i class="fa fa-buysellads" aria-hidden="true"></i></td>
														<td>Topic</td>
														<td>Lecture IT</td>
													</tr>
													<tr>
														<td><i class="fa fa-calendar" aria-hidden="true"></i></td>
														<td>Start</td>
														<td>17 jun 16</td>
													</tr>
													<tr>
														<td><i class="fa fa-times" aria-hidden="true"></i></td>
														<td>End</td>
														<td>19 jun 16</td>
													</tr>
													<tr>
														<td><i class="fa fa-check-square" aria-hidden="true"></i></td>
														<td>Start Time</td>
														<td>12pm</td>
													</tr>
													<tr>
														<td><i class="fa fa-times" aria-hidden="true"></i></td>
														<td>End Time</td>
														<td>05am</td>
													</tr>
													<tr>
														<td><i class="fa fa-users" aria-hidden="true"></i></td>
														<td>Students</td>
														<td>120</td>
													</tr>
													<tr>
														<td><i class="fa fa-thumbs-up" aria-hidden="true"></i></td>
														<td>Cost</td>
														<td>Free</td>
													</tr>
													<tr>
														<td><i class="fa fa-adn" aria-hidden="true"></i></td>
														<td>Language</td>
														<td>English</td>
													</tr>
												</table>
											</div>
										</div>
	        						</div>
	        					</div> <!-- /.sub-text -->

	        					<div class="sub-text">
	        						<h4>ORGANIZER</h4>
	        						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard </p>
	        						<ul class="organize-list">
	        							<li><i class="fa fa-envelope-o" aria-hidden="true"></i> backpiper.info@gmail.com</li>
	        							<li><i class="fa fa-phone" aria-hidden="true"></i> +8801682648101</li>
	        							<li><i class="fa fa-calendar" aria-hidden="true"></i> Economic Analysis</li>
	        							<li><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Professional MCC (UK)</li>
	        							<li><i class="fa fa-location-arrow" aria-hidden="true"></i> themeforest.net</li>
	        						</ul>
	        					</div> <!-- /.sub-text -->
	        					<hr>
	        					<div class="sub-text clear-fix">
	        						<h6 class="float-left">Share</h6>
	        						<ul class="float-right share-icon">
										<li><a href="#" class="tran3s round-border icon"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				        				<li><a href="#" class="tran3s round-border icon"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
										<li><a href="#" class="tran3s round-border icon"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
									</ul>
	        					</div> <!-- /.sub-text -->
	        				</div> <!-- /.event-details-content -->
	        			</div> <!-- /.col- -->
				<!-- _________________ SideBar _________________ -->
	        			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 sidebarOne float-left">
	        				<div class="wrapper">
	        					<div class="sidebar-box quick-event-list">
	        						<div class="box-wrapper">
	        							<h4>QUICK event List</h4>
	        							<ul>
	        								<li><a href="" class="tran3s"><i class="fa fa-angle-double-right" aria-hidden="true"></i> CORPORATE EVENT</a></li>
	        								<li class="active"><a href="" class="tran3s"><i class="fa fa-angle-double-right" aria-hidden="true"></i> ECONOMIC EVENT</a></li>
	        								<li><a href="" class="tran3s"><i class="fa fa-angle-double-right" aria-hidden="true"></i> LAW AND ECONOMICS</a></li>
	        								<li><a href="" class="tran3s"><i class="fa fa-angle-double-right" aria-hidden="true"></i> THE SECRETS OF ECONOMIC</a></li>
	        								<li><a href="" class="tran3s"><i class="fa fa-angle-double-right" aria-hidden="true"></i> CONTENT STRATEGY</a></li>
	        								<li><a href="" class="tran3s"><i class="fa fa-angle-double-right" aria-hidden="true"></i> IT & SOFTWARE</a></li>
	        								<li><a href="" class="tran3s"><i class="fa fa-angle-double-right" aria-hidden="true"></i> TEXT STRATEGY</a></li>
	        								<li><a href="" class="tran3s"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Finance rss</a></li>
	        							</ul>
	        						</div> <!-- /.box-wrapper -->
	        					</div> <!-- /.sidebar-box.quick-event-list -->

	        					<div class="sidebar-box feature-event">
	        						<div class="box-wrapper">
	        							<h4>Featured events</h4>
	        							
	        							<div class="single-event clear-fix">
	        								<div class="date float-left p-color-bg">
	        									27 <span>Feb</span>
	        								</div> <!-- /.date -->
	        								<div class="post float-left">
	        									<a href="#" class="tran3s">The multi-group recital included jolly</a>
	        									<ul>
	        										<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10pm-04pm</li>
	        										<li><i class="fa fa-tag" aria-hidden="true"></i>Golf Club</li>
	        									</ul>
	        								</div> <!-- /.post -->
	        							</div> <!-- /.single-event -->

	        							<div class="single-event clear-fix">
	        								<div class="date float-left p-color-bg">
	        									21 <span>Feb</span>
	        								</div> <!-- /.date -->
	        								<div class="post float-left">
	        									<a href="#" class="tran3s">The multi-group recital included jolly</a>
	        									<ul>
	        										<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10pm-04pm</li>
	        										<li><i class="fa fa-tag" aria-hidden="true"></i>Golf Club</li>
	        									</ul>
	        								</div> <!-- /.post -->
	        							</div> <!-- /.single-event -->

	        							<div class="single-event clear-fix">
	        								<div class="date float-left p-color-bg">
	        									17 <span>Feb</span>
	        								</div> <!-- /.date -->
	        								<div class="post float-left">
	        									<a href="#" class="tran3s">The multi-group recital included jolly</a>
	        									<ul>
	        										<li><i class="fa fa-clock-o" aria-hidden="true"></i> 10pm-04pm</li>
	        										<li><i class="fa fa-tag" aria-hidden="true"></i>Golf Club</li>
	        									</ul>
	        								</div> <!-- /.post -->
	        							</div> <!-- /.single-event -->
	        						</div> <!-- /.box-wrapper -->
	        					</div> <!-- /.sidebar-box.feature-event -->

	        					<div class="sidebar-box our-brochure">
	        						<div class="box-wrapper">
	        							<h4>Download brochure</h4>
	        							<a href="" class="tran3s"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download pdf</a>
	        							<a href="" class="tran3s"><i class="fa fa-file-word-o" aria-hidden="true"></i> Download doc</a>
	        							<a href="" class="tran3s"><i class="fa fa-file-word-o" aria-hidden="true"></i> Download doc</a>
	        							
	        						</div> <!-- /.box-wrapper -->
	        					</div> <!-- /.sidebar-box.our-brochure -->
	        				</div> <!-- /.wrapper -->
	        			</div> <!-- /.sidebarOne -->
	        		</div>
	        	</div>
	        </div>




			<!-- SubsCribe Banner ___________________ -->
	        <div class="subscribe-banner p-color-bg wow fadeInUp">
	        	<div class="container">
	        		<h3>Subscribe now</h3>
	        		<p>Receive weekly newsletter with educational materials, new courses, most popular posts, popular books and much more!</p>
	        		<form action="#" class="clear-fix">
	        			<input type="email" class="float-left wow fadeInLeft" placeholder="Email address">
	        			<button class="float-left tran3s wow fadeInRight">Subcribe</button>
	        		</form>
	        	</div>
	        </div>
@endsection

