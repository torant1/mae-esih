@extends('layouts.master_home')
@section('content')
			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2 class="text-center">La recherche à l'IAE NICE et à l’ESIH</h2>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
						<ul>
						<li><a href="index">Accueil</a></li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Formation</li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Recherche</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->

			<!-- Event Section _______________________ -->
	        <div class="recherche-section">
	        	<div class="container">
					<div class="row">
						<div class="text-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<p>
								La recherche fait partie des priorités stratégiques de l'IAE de NICE et de l’ESIH. Par ce
								programme, les étudiants intéressés et motivés pourront aspirer à réaliser une thèse de
								doctorat au sein d’un groupe de recherche :
							</p>
							<br>
							<p style="text-indent: 1.0em;">
								A l’IAE de Nice dont les trois axes de recherche sont : 
								<ul>
								    <li > <span class="text-list">Normes comptables et indicateurs de performance ,</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="1s"> <span class="text-list">Marketing digital et co-création de valeur ,</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="2s"><span class="text-list">Management responsable des organisations. </span></li>
								
								</ul>
								<br>

							<p>
								<span class="style-link "> <a target="_blank"  href="http://unice.fr/iae/fr/recherche">Cliquez sur ce lien <i class="fa fa-long-arrow-right fa-link wow wobble" data-wow-duration="3s"></i></a></span>
							</p>
								
							</p>	
							 <br>
							 <p style="text-indent: 1.0em;">
							 	Au laboratoire SITERE de l'ESIH dont les axes de recherche: 
								<ul>
								    <li> <span class="text-list">Simulation et systèmes intelligents ,</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="1s"> <span class="text-list">Réseaux et Télécommunications ,</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="2s"> <span class="text-list">Réalité virtuelle. </span></li>
								 </ul>
								 <br>
								 <p><span class="style-link"> <a target="_blank" href="http://sitere.science/">Cliquez sur ce lien <i class="fa fa-long-arrow-right fa-link wow wobble" data-wow-duration="3s"></i></a></span></p>
								
							 </p>	
						</div>
					    			
					</div>    		
	        	</div>
	        </div>
	  
@endsection