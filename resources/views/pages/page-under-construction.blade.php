@extends('layouts.master_home')
@section('content')
		<div class="main-page-wrapper about-us-v1-page">
			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2>cette page est en construction</h2>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->

			<!-- Error Page _________________________ -->
			<div class="error-page" style="margin-top: 50px;">
				<div class="container">
					<div class="error-wrapper clear-fix">
						<img src="https://tractortestlab.unl.edu/image/image_gallery/uuid%3Df04051c0-3ef9-4cd2-b01c-480203d38f41%26groupId%3D4805395%26t%3D1361553219293" alt="" class="float-left wow fadeInLeft">
						<div class="text float-right wow fadeInRight">
							<h2 ><span class="p-color "></span> <span class="last p-color" style="font-size: 30px;">page en construction</span></h2>
							<p>Cette page est en construction</p>
							<a href="index" class="tran3s"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>Page d'accueil</a>
						</div> <!-- /.text -->
					</div> <!-- /.error-wrapper -->
				</div> <!-- /.container -->
			</div> <!-- /.error-page -->

	    </div>

@endsection