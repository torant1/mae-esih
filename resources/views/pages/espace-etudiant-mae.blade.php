@extends('layouts.master_home')
@section('content')
			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2 class="text-center">MAE-DE, UNE FORMATION A PORTEE DE MAIN</h2>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
					<ul class="float-left">
						<li><a href="index">Accueil</a></li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Etudiant(e)s</li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Espace Etudiant(e)s</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->

			<!-- Event Section _______________________ -->
	        <div class="partenaire-section wow fadeInUp">
	        	<div class="container">
					<div class="row">
						<div class="text-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<h2 class=" text-bold text-title">Tout au long de votre parcours au MAE_ESIH, vous pourrez accéder</h2>
							
								<p><i class="fa fa-long-arrow-right fa-bullet wow fadeInLeft" data-wow-delay="1s"></i><span class=" text-bold">A un espace pédagogique sous la forme d’un environnement d’apprentissage en ligne (EAL) dédié spécialement aménagé pour vous</span></p>
								<br>
							<P>
							<h4 style="text-indent: 2.0em;">Le MAE_ESIH utilise un espace sur la plateforme THESS développée par l’ESIH grâce à un financement de l’Ambassade de France. THESS hébergera vos contenus en ligne et vous permettra, pour chaque cours ou UE, d’accéder, jour et nuit, avec aisance à un grand volume de supports pédagogiques. Vous pourrez vous y connecter par le lien THESS online et accéder à l’aide de vos identifiants.</h4>
							</P>
						<br>
								<p><i class="fa fa-long-arrow-right fa-bullet wow fadeInLeft" data-wow-delay="1s" ></i><span class=" text-bold">Au planning de vos cours</span></p>
								<br>
							<P>
							<h4 style="text-indent: 2.0em;">Vous pourrez ainsi gérer votre emploi du temps, notamment les semaines de cours, d’examens ainsi que les activités connexes auxquels vous êtes invités à participer.</h4>
							</P>
						<br>
								<p><i class="fa fa-long-arrow-right fa-bullet wow fadeInLeft" data-wow-delay="1s" ></i><span class=" text-bold">Accéder aux ressources documentaires pour vos cours et vos projets : 2 types de bibliothèques, une physique et une virtuelle.</span></p>
								<br>
							{{-- <P>
							<h4 style="text-indent: 2.0em;">L'accès à la bibliothèque est contrôlé, vous devez donc avoir sur vous, votre carte d’étudiant</h4>
							</P> --}}
						</div>
					    			
					</div>    		
	        	</div>
	        </div>
	  
@endsection