@extends('layouts.master_home')
@section('content')

			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2 class="text-center">Mots de la direction</h2>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum mots-direction-ancre">
				<div class="container">
					<ul class="float-left  ">
						<li><a href="index">Accueil</a></li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>A propos</li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Mots de la direction</li>
					</ul>
					<ul class="float-right  ">
						<li><a href="#motsPatric">Pattrick Atti&eacute;</a></li>
						<li><i class="fa fa-caret-down"></i></li>
						<li><a href="#motsNardine">Nadine Tournois</a></li>
						<li><i class="fa fa-caret-down"></i></li>
						<li><a href="#motsArnaud">Arnaud Langlois-Meurinne</a></li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->


	        <!-- Teacher Profile Page  ___________________ -->
	        <div class="teacher-profile" style="padding-bottom: 40px !important; " id="motsPatric">
	        	<div class="container">
	        		<div class="row">
	        			<div class=" col-lg-8 col-md-8 col-sm-12 col-xs-12 flot-right  teacher-speach">
	        				<div class=" text-warpper">
	        					<h4 class="text-uppercase">Ecole Superieure d'infotronique d'Haiti</h4>
	        						<div class="teacher-sidebar oval" style="display: inline-block;">
	        							<div class="img-holder theme-bg-color"><img src="https://i.pinimg.com/originals/8e/ce/bb/8ecebb7051cc7e7bc50fccb55cc97c8d.jpg" alt="Patrick Attie"></div>

	        						</div>

	        					<p class="mmfix " > 
	        					
	        						{{-- <i class="fa fa-quote-left"></i> --}}
	        					Patrick Attié détient un diplôme d’Ingénieur en électronique de l’Ecole Supérieure d’Electronique de l’Ouest (ESEO), Angers, France ainsi qu’un Master en Sciences de l’Informatique de l’Université Georges Washington, Etat de Washington D.C. aux Etats-Unis.
	        						<br> 
	        					Il est actuellement Directeur de l’Ecole Supérieure d’Infotronique d’Haïti (ESIH), un établissement privé d’enseignement Supérieur, reconnu par le Ministère de l’Education Nationale et de la formation Professionnelle (MENFP) et membre votant de l’Agence Universitaire de la Francophonie (AUF) et dont il est aussi l’un des fondateurs.
	        					<br>
	        					L’ESIH a introduit le premier TEDx à Port au Prince et à Jacmel et le premier Hackathon en Haïti.
	        					<br>
	        					Patrick Attié est également co-fondateur du premier laboratoire caribéen de réalité virtuelle et de modélisation immersive, qui élabore actuellement le premier modèle 3D immersif de la Citadelle Laferrière, au Cap Haïtien.
	        					<br>
	        					Dans le monde associatif, il a été Vice-président de la Chambre Franco-Haïtienne de Commerce et d’Industrie (CFHCI) pendant plus de 6 ans, membre fondateur de la “Conférence des Recteurs et des Présidents des Universités Haïtiennes” (CORPUHA) et président de l’Association Haïtienne pour le développement des TIC (AHTIC) de 2013 à 2015.
	        					<br>
	        					Il a été le coordonnateur du Groupe de Travail sur les Technologies de l’information et de la Communication (GTIC) de 2009 à 2011.
	        					<br>
	        					La vision de Patrick est celle d’un écosystème d’enseignement supérieur économiquement fort, disposant de compétences pointues en recherche scientifique, producteur de valeur ajoutée et mené par une culture entrepreneuriale. C’est le seul moyen, selon lui, de fournir une éducation de pointe à un plus grand nombre de jeunes individus haïtiens, et de produire cette nouvelle génération d’hommes et de femmes ayant une vision moderne, voire futuriste du monde, et dont ce pays a tant besoin. Ceci, bien sûr, si nous voulons prétendre, un jour, devenir un acteur crédible du processus mondial d’élaboration des stratégies qui décideront du futur de la planète en général, et du notre en particulier !
	        					{{-- <i class="fa fa-quote-right"></i> --}}
	        				</p>

	        				</div> <!-- /.text-wrapper -->
	        			</div>

	        			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 teacher-sidebar float-left wow fadeInUp">
	        				<div class="teachers-bio p-color-bg">
	        					<h6>Patrick ATTIE</h6>
	        					<p><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Directeur Général de l'ESIH</p>
{{-- 	        					<p><i class="fa fa-graduation-cap" aria-hidden="true"></i>Ing&eacute;nieur en &eacute;lectronique</p>
 --}}	        					<p><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:patrick.attie@esih.edu">patrick.attie@esih.edu</a></p>
	        					<p><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:+50922264749">(509)  22 26 47 49</a></p>
	        					<p><i class="fa fa-location-arrow" aria-hidden="true"></i> <a target="_blank" href="http://www.esih.edu">www.esih.edu</a></p>

	        					<ul>
									<li><a href="https://www.facebook.com/Patrickattieesihedu-243643765747567/" class="tran3s round-border icon"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			        				<li><a href="https://www.linkedin.com/in/patrickattie" class="tran3s round-border icon"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
									<li><a href="https://plus.google.com/104051027136273804792" class="tran3s round-border icon"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
{{-- 									<li><a href="#" class="tran3s round-border icon"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
 --}}								</ul>
	        				</div> <!-- /.teachers-bio -->
	        			</div> <!-- /.teacher-sidebar -->
	        		</div> <!-- /.row -->
	        	</div> <!-- /.container -->
	        </div> <!-- /.teacher-profile -->
	         <!-- Teacher Profile Page  ___________________ -->
	        <div class="teacher-profile" style="padding-bottom: 30px; background-color: #F4F7F9; padding-top: 8%;" id="motsNardine">
	        	<div class="container">
	        		<div class="row">
	        			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 flot-right  teacher-speach">
	        				<div class=" text-warpper">
	        					<h4 class="text-uppercase">Universite de Nice </h4>
	        						<div class="teacher-sidebar oval" style="display: inline-block;">
	        							<div class="img-holder theme-bg-color"><img src="https://i.pinimg.com/originals/5b/a9/79/5ba979403378b065c392a784e345ae03.jpg" alt="Teacher"></div>
	        						</div>
	        					<p class="mmfix " > 
	        					
	        						
	        					Nadine Tournois a suivi des études universitaires en sciences économiques et en Sciences de Gestion. En 1978,
								elle devient analyste financier à Texas Instrument puis a l’opportunité d’entrer à IBM, en tant qu’ingénieur
								technico-commercial.
	        						<br> <br>
	        					Nadine Tournois se tourne rapidement vers l’enseignement, passe des concours, et intègre l'Université Nice
								Sophia Antipolis d'abord comme Maître de conférences des universités en 1984 puis comme Professeur des
								Universités en 1993. Elle occupe différents postes dont celui de Chef du département Information-
								Communication de l’IUT Nice, Côte d’Azur en 1995. En 2002, elle prend une première fois la direction de
								l’Institut d’Administration des Entreprises (IAE) de Nice, poste qu’elle quitte pour devenir Vice-Président du
								Conseil d’Administration de l’Université Nice Sophia Antipolis en 2004. Par ailleurs, Nadine Tournois crée divers
								programmes internationaux de l’IAE dont le Master « Affaires et Management International ».
	        					<br>
	        					Au sein de l’Académie de Nice, en 2009, elle devient Directeur de l’Enseignement Supérieur et de la Recherche,
								et conseillère du Recteur avant de redevenir Directeur de l’IAE en 2013.
	        					<br>
	        					Nadine Tournois a reçu de nombreuses récompenses. En 2007, elle est faite Chevalier de l’ordre des Palmes
								Académiques et en 2010, Chevalier de l’Ordre National du Mérite. Aujourd’hui, elle reçoit l’Insigne de Chevalier
								de l’Ordre National de la Légion d’honneur..
	        				</p>

	        				</div> <!-- /.text-wrapper -->
	        			</div>

	        			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 teacher-sidebar float-left wow fadeInUp">
	        				<div class="teachers-bio p-color-bg">
	        					<h6>Nadine Tournois</h6>
	        					<p><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Directeur de l'Institut d'Administration des
Entreprises de Nice </p>
	        					<p><i class="fa fa-pencil-square-o" aria-hidden="true"></i> membre du conseil d'administration de l'UNS</p>
	        					<p><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:nardine.tournois@email.com">nardine.tournois@email.com</a></p>
	        					{{-- <p><i class="fa fa-phone" aria-hidden="true"></i> <a href="#">+8801712570051</a></p> --}}
	        					<p><i class="fa fa-location-arrow" aria-hidden="true"></i> <a target="_blank" href="http://unice.fr/iae/fr/lecole/accueil">www.unice.fr</a></p>

	        					<ul>
									<li><a href="https://www.linkedin.com/in/nadine-tournois-7a07b05b/" class="tran3s round-border icon"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
			        				{{-- <li><a href="#" class="tran3s round-border icon"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a href="#" class="tran3s round-border icon"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
									<li><a href="#" class="tran3s round-border icon"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li> --}}
								</ul>
	        				</div> <!-- /.teachers-bio -->
	        			</div> <!-- /.teacher-sidebar -->
	        		</div> <!-- /.row -->
	        	</div> <!-- /.container -->
	        </div> <!-- /.teacher-profile -->
	         <!-- Teacher Profile Page  ___________________ -->
	        <div class="teacher-profile" style="padding-bottom: 30px; padding-top: 8%;" id="motsArnaud">
	        	<div class="container">
	        		<div class="row">
	        			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 flot-right  teacher-speach">
	        				<div class=" text-warpper">
	        					<h4 class="text-uppercase">FNEGE</h4>
	        						<div class="teacher-sidebar oval" style="display: inline-block;">
	        							<div class="img-holder theme-bg-color"><img src="https://i.pinimg.com/originals/55/9f/c0/559fc021e521ac29f637ea94dc5a9a2f.jpg" alt="Teacher"></div>
	        						</div>
	        					<p class="mmfix " > 
	        					Rouen (renommé Rouen Business School depuis juin 2009) de 2002 à 2012.
	        						<br> <br>
	        					Diplômé de HEC (option marketing) en 1970, Arnaud Langlois-Meurinne obtient ensuite une maîtrise de sociologie. Il entre aux Editions Nathan en tant que chef de produits audiovisuels en 1979, puis devient directeur général de la filière éducation en 1996. Il ne quittera plus le secteur de l’éducation. 
	        					<br>
	        					En 1998, il est nommé Directeur Général d’Havas éducation et référence, pour le pôle éducation et préside l’association des éditeurs scolaires, Savoir Lire. Arnaud Langlois-Meurinne fonde, en 2001, la société de conseil et formation Education et Territoires. 
	        					<br>
	        					Il rejoint l’année suivante le groupe ESC Rouen, avec pour mission de réunir sous une même bannière quatre écoles de la CCI de Rouen (ESC Rouen, IFI, ISPP, ECAL) et de conduire un plan de développement pour le groupe.
	        				</p>

	        				</div> <!-- /.text-wrapper -->
	        			</div>

	        			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 teacher-sidebar float-left wow fadeInUp">
	        				<div class="teachers-bio p-color-bg">
	        					<h6>Arnaud LANGLOIS-MEURINNE</h6>
	        					{{-- <p><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Science and Technology</p> --}}
	        					<p><i class="fa fa-pencil-square-o" aria-hidden="true"></i>membre du groupe ESC Rouen</p>
	        					<p><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:langlois@mae_esih.com">arnaud.langlois@mae_esih.com</a></p>
	        					{{-- <p><i class="fa fa-phone" aria-hidden="true"></i> <a href="#">+8801712570051</a></p> --}}
	        					<p><i class="fa fa-location-arrow" aria-hidden="true"></i> <a target="_blank" href="http://www.fnege.org/">www.fnege.org</a></p>

	        					<ul>
									<li><a href="https://fr-fr.facebook.com/arnaud.langloismeurinne" class="tran3s round-border icon"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			        				<li><a href="https://www.linkedin.com/in/arnaud-langlois-meurinne-5889056/" class="tran3s round-border icon"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
									{{-- <li><a href="#" class="tran3s round-border icon"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
									<li><a href="#" class="tran3s round-border icon"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li> --}}
								</ul>
	        				</div> <!-- /.teachers-bio -->
	        			</div> <!-- /.teacher-sidebar -->
	        		</div> <!-- /.row -->
	        	</div> <!-- /.container -->
	        </div> <!-- /.teacher-profile -->
@endsection