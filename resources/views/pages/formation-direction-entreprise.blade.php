@extends('layouts.master_home')
@section('content')
			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2 class="text-center">Master 2 en administration des entreprises de l'iae de nice </h2>
						<h3 class="text-inner-second">Sp&eacute;cialit&eacute; direction des entreprises d&eacute;localis&eacute;e &agrave; l'ESIH , &agrave; Port-au-prince en Ha&iuml;ti</h3>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
					<ul>
						<li><a href="index">Accueil</a></li>
						<li><li><i class="fa fa-caret-right"></i></li></li>
						<li>Formation</li>
						<li><li><i class="fa fa-caret-right"></i></li></li>
						<li>MAE-DE</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->



			<!-- Event Section _______________________ -->
	        <div class="event-section wow fadeInUp" style="margin-top:-10em;">
	        	<div class="container">
	        		<div class="row">
	        		{{-- 	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        					<div class="sidebar-box our-brochure">
	        						<div class="box-wrapper">
	        							<h4>Download brochure</h4>
	        							<a href="" class="tran3s"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download pdf</a>
	        							<a href="" class="tran3s"><i class="fa fa-file-word-o" aria-hidden="true"></i> Download doc</a>
	        							<a href="" class="tran3s"><i class="fa fa-file-word-o" aria-hidden="true"></i> Download doc</a>
	        							
	        						</div> <!-- /.box-wrapper -->
	        					</div> <!-- /.sidebar-box.our-brochure -->
	        			</div> --}}
	        			<div class=" col-lg-12 col-md- col-sm-12 col-xs-12 ">
	        				<div class="">
	        					<ul class="row nav nav-tabs" id="myTab">
									  <li class="col-lg-3 col-md-3 col-sm-12 col-xs-12 single-tab-formation theme-bg-color active text-uppercase"><a href="#home">presentation</a></li>
									  <li class="col-lg-3 col-md-3 col-sm-12 col-xs-12 single-tab-formation theme-bg-color text-uppercase"><a href="#admission">admission</a></li>
									  <li class=" col-lg-3 col-md-3 col-sm-12 col-xs-12 single-tab-formation theme-bg-color text-uppercase"><a href="#programme">programme</a></li>
									  <li class="col-lg-3 col-md-3 col-sm-12 col-xs-12 single-tab-formation theme-bg-color text-uppercase"><a href="#debouche">debouches</a></li>
									</ul>
									 
									<div class="text-content tab-content">
									  <div class="tab-pane active  " id="home"> 
									  	<h4 class="text-formation-title text-bold " style="margin-top: 20px; ">Pr&eacute;sentation</h4>
									  	<div class="text-justify" style="margin-top: 20px; margin-bottom: 10px;">
									  			  <p> La spécialité MAE Direction d'Entreprises est un Master qui a évolué au fil des années. Elle fait suite au DESS CAAE, à la spécialité C.A.A.E et à la spécialité Administration des
													Entreprises (MAE). Cette formation est sanctionnée par un diplôme reconnu et apprécié par
													les entreprises et institutions publiques et privées, car il apporte une réponse à leurs attentes.</p>
													<br>
													<p>
														L’objectif de cet enseignement de haut niveau est de former des cadres et des managers dynamiques capables de concilier un savoir-faire spécialisé et une compétence générale et
										opérationnelle en gestion. Il familiarise les étudiant(e)s aux méthodes et techniques de la
										gestion des entreprises d’aujourd’hui et de demain. Le MAE-DE s’adresse à des étudiant(e)s
										provenant d’horizons multiples : ingénieurs, scientifiques, juristes, diplômés des filières
										médicales et paramédicales, sociologues et psychologues, architectes, littéraires et aux
										cadres déjà en activité ou en recherche d’emploi qui souhaitent donner une nouvelle
										impulsion à leur carrière.
													</p>
									  
									  	<h4 class=" text-formation-title text-bold" style="margin-top: 20px; ">Compétences</h4>
									  	
									  			  <p>La formation est axée sur la double compétence. Le MAE Direction d’Entreprises propose : 
									  		<ul>
									 	    <li><span class="text-list"> L’apprentissage aux méthodes et techniques de management des entreprises ;</span></li>
									 	    <li class="wow fadeInLeft" data-wow-delay="1s"> <span class="text-list"> L’élaboration de diagnostic et de choix stratégiques dans une optique nationale,internationale, et mondiale ;</span></li>
									 	    <li class="wow fadeInLeft" data-wow-delay="2s"><span class="text-list"> L’apprentissage des principales techniques financières et outils de gestion ;</li>
									 	    <li class="wow fadeInLeft" data-wow-delay="3s"><span class="text-list"> Le développement des aptitudes managériales, des connaissances en marketing, droit, gestion des ressources humaines et en management des systèmes d’information.</span></li>
									 	</ul>
									 		</div>
									</div>
									{{-- Div Admission --}}
									  <div class="tab-pane" id="admission"> 
									  	<h4 class="text-formation-title text-bold " style="margin-top: 20px; ">Conditions</h4>
									  	<div class="text-justify" style="margin-top: 20px; margin-bottom: 10px;">
									  			 <p>
									  			  <ul>
									  			       <li><span class="text-list"> Etre titulaire d'un BAC + 4 ou d'un diplôme d’ingénieur, scientifique, juridique, ….</span></li>
									  			       <li class="wow fadeInLeft" data-wow-delay="1s"><span class="text-list"> Justifier d'au moins 36 mois d'expérience professionnelle serait souhaitable</span></li>
									  			     
									  			   </ul>
									  	<h4 class="text-formation-title text-bold " style="margin-top: 20px; ">Candidature</h4>
									<p>Lors du dépôt de votre candidature, nous vous invitons à :</p>
								<ul>
								    <li><span class=" text-list">Compléter votre profil ;</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="1s"><span class=" text-list"> Choisir vos formations ;</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="2s"><span class=" text-list">Envoyer les documents pour compléter votre dossier. </span></li>
								</ul> 
									  	<h4 class="text-formation-title text-bold " style="margin-top: 20px; ">Admissibilit&eacute;</h4>
									  <ul>
								    <li><span class=" text-list">Les dossiers sont examinés par le Comité de sélection binational (IAE-Nice et ESIH).</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="1s"><span class=" text-list"> Le paiement de frais d’examen de dossier : 7500 Gourdes</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="2s"><span class=" text-list"> Les candidats présélectionnés sur dossier comme étant "admissibles" sont invités par e-mail à un entretien individuel avec le Jury.</span></li>		
								</ul>
									  			  <br>
									  	<h4 class="text-formation-title text-bold " style="margin-top: 20px; ">Entretien</h4>
								<br>
							<p>
								<ul>
								    <li><span class=" text-list">  Les entretiens d'admission avec les membres du Comité de sélection constitué en Jury ont lieu dans les locaux du MAE_ESIH.</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="2s"><span class=" text-list">Votre convocation par courrier électronique indique la date et l'heure exactes d'entretien du candidat ou de la candidate.</span></li>		
								</ul>

							</p>
							<br>
								<p>
									<h4 class="text-center text-bold text-title" style="text-decoration: underline;">La présence à l’entretien est obligatoire.</h4>
								</p>
							<p>
								
								L'entretien peut exceptionnellement se dérouler par vidéo-conférence, avec l’accord express du Jury. Les modalités et conditions pour la demande d'entretien à distance seront indiquées dans le message électronique de convocation. A noter que les entretiens téléphoniques (sans contact vidéo) sont strictement interdits, et de ce fait, exclus.
								<br>
								<br>
								Lors de cet entretien vous serez amené (e) à exposer vos motivations, votre plan de carrière, ainsi que la maîtrise que vous avez du langage technique et managérial de l’entreprise. 
							</p>
							<br>
							<br>
								<h4 class="text-formation-title text-bold " style="margin-top: 20px; ">Admission</h4>
								<br>

							<p  >
								Les candidats retenus à la suite de l’entretien sont déclarés "admis" et reçoivent un courrier électronique. Les candidats déclarés "admis" doivent confirmer leur admission dans un délai de 10 jours.
							</p>

								<br>
								<p >
								<span class="text-red">Nota Bene :</span> Veuillez noter que les places disponibles pour chaque formation sont limitées (30). Les candidats admis sur liste d'attente ne seront contactés qu’en cas de désistement d'un candidat admis. 
								</p>
								<br>
								<p >
									<span class="text-bold" style="color: red; text-decoration: underline;">Attention :</span> Le document original est envoyé par défaut à l'adresse fournie lors de la candidature (celle qui apparaît dans votre "profil"). Si vous souhaitez que toute correspondance avec vous soit envoyée à une adresse différente, veillez à le spécifier dans la rubrique demande d'envoi.
								</p>
							<br>
							<br>
							<h4 class="text-formation-title text-bold " style="margin-top: 20px; ">Confirmation</h4>
								<br>
								<p>
									Si vous êtes admis, vous devez impérativement confirmer votre souhait d'être inscrit(e) dans un délai de 10 jours. Passé ce délai, votre place est irrévocablement proposée aux candidat(e)s sur liste d'attente.
									<br>
									La confirmation se fait exclusivement en ligne via votre profil, en suivant les instructions contenues dans le mail d'admission.

								</p>
								<br>
								<p>
									Une fois que vous aurez confirmé, vous recevrez un message vous transmettant les informations qui vous permettrons de parachever votre inscription. Vous pourrez être amené(e) à produire des pièces complémentaires et, le cas échéant, un justificatif de paiement de vos droits de scolarité.
								</p>
								<br>
								<p>
									<span class="text-bold" style="color: red; text-decoration: underline;">Attention :</span> Votre place n’est sécurisée qu’à partir du moment où l’Economat reçoit votre confirmation/inscription et preuve du paiement de tout ou partie de vos frais scolaires en fonction des modalités de paiement arrêtées pour le MAE_ESIH. En cas de non-respect des délais et échéances de la part d’un candidat admis, un(e) candidat(e) sur liste d'attente se verra offrir l’opportunité de prendre sa place.
								</p>
									  	<h4 class="text-formation-title text-bold " style="margin-top: 20px; ">Coût de la formation</h4>
									  			 <ul>
									  			     <li><span class="text-list"> Frais de formation : 10 000 $US (ou l’équivalent en Gourdes) / Les droits universitaires et livres de référence inclus</span></li>
									  			     
									  			 </ul>

									  			 <br>
									  			  <br>
									  			  <h4 class="style-link"><a href="#!">S'inscrire <i class="fa fa-long-arrow-right fa-link" ></i></a></h4>

									  	{{-- 	<H3 class="text-formation-title text-bold ">Documents &agrave; t&eacute;l&eacute;charger</H3>
									  		<ul>
									  		  <a href="#!" class="tran3s"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
	        							<a href="#!" class="tran3s"><i class="fa fa-file-word-o" aria-hidden="true"></i></a>	        							
									  		</ul> --}}
									  	</p>
									  	</div>
									   </div>
									   {{-- Div programme --}}
									  <div class="tab-pane" id="programme">
									  
									  {{-- tableaux premier semestre de cours --}}
									  <div class="course-details-page">
									  	<div class=" course-details-content">
									  		<div class="sub-text course-curriculum wow fadeInUp " style=" margin-top: 12em;">
	        						<div class="single-panel-area">
	        							<h6>PREMIER SEMESTRE</h6>
	        							<div class="course-curriculum-panel">
											<div class="panel-group theme-accordion" id="accordion">
												<div class="panel">
											    <div class="panel-heading">
											      <div class="panel-title bg-danger">
											        <a  >
											        	<table style="width: 100%;">
											        		<tr>
											        			<td>COURS </td>
											        			<td><div class="p-color-bg">ECTS</div></td>
											        			<td ><div class="p-color-bg">HEURES</div></td>
											        		</tr>
											        	</table>
											        </a>
											      </div>
											    </div>
											  
											  </div>
											  <div class="panel">
											    <div class="panel-heading">
											      <div class="panel-title">
											        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
											        	<table style="width: 100%;">
											        		<tr>
											        			<td> UE 1 Manager les organisations

 </td>
											        			<td><div class="p-color-bg">6</div></td>
											        			<td ><div class="p-color-bg">72</div></td>
											        		</tr>
											        	</table>
											        </a>
											      </div>
											    </div>
											    <div id="collapse1" class="panel-collapse collapse">
											      <div class="panel-body">
											      	<p>
											      		<ul>
											      		    <li><span class="text-list">Dynamiques des organisations (24 HRS CM)</span> </li>
											      		    <li><span class="text-list">Stratégie (24 HRS CM) </span> </li>
											      		    <li><span class="text-list">Environnement et enjeux internationaux (24 HRS CM) </span></li>
											      		</ul>
											      	</p>
											      	{{-- <a href="details-cours" class="tran3s"><i class="fa fa-angle-double-right" aria-hidden="true"></i> lire details cours</a> --}}
											      </div>
											    </div>
											  </div> <!-- /panel 1 -->

											  <div class="panel">
											    <div class="panel-heading">
											      <div class="panel-title">
											        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
											        	<table style="width: 100%;">
											        		<tr>
											        			<td>  UE 2 Manager la performance comptable et financière

</td>
											        			<td><div class="p-color-bg">6</div></td>
											        			<td ><div class="p-color-bg">60</div></td>
											        		</tr>
											        	</table>
											        </a>
											      </div>
											    </div>
											    <div id="collapse2" class="panel-collapse collapse">
											      <div class="panel-body">
											      	<p>
											      		<ul>
											      		    <li><span class="text-list">Comptabilité  (12 HRS CM) </span></li>
											      		    <li><span class="text-list">Contrôle de gestion (24 HRS CM)</span>  </li>
											      		    <li><span class="text-list">Analyse comptable et financière  (24 HRS CM) </span></li>
											      		</ul>
											      	</p>
											      {{-- 	<a href="details-cours" class="tran3s"><i class="fa fa-angle-double-right" aria-hidden="true"></i> lire details cours</a> --}}
											      	{{-- <a href="resource/1.docx" class="tran3s"><i class="fa fa-file-word-o" aria-hidden="true"></i> Download doc</a> --}}

											      </div>
											    </div>
											  </div> <!-- /panel 2 -->
											  <div class="panel">
											    <div class="panel-heading">
											      <div class="panel-title">
											        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
											        	<table style="width: 100%;">
											        		<tr>
											        			<td>  UE 3 Manager les ressources humaines et la communication </td>
											        			<td><div class="p-color-bg">3</div></td>
											        			<td ><div class="p-color-bg">48</div></td>
											        		</tr>
											        	</table>
											        </a>
											      </div>
											    </div>
											    <div id="collapse3" class="panel-collapse collapse">
											      <div class="panel-body">
											      	<p>
											      		<ul>
											      		    <li><span class="text-list">Communication d'entreprise   (12 HRS CM)</span> </li>
											      		    <li><span class="text-list">Animation d'équipe  (12 HRS CM)</span>  </li>
											      		    <li><span class="text-list">Gestion des ressources humaines   (24 HRS CM)</span> </li>
											      		</ul>
											      	</p>
											      	{{-- <a href="details-cours" class="tran3s"><i class="fa fa-angle-double-right" aria-hidden="true"></i> lire details cours</a> --}}
											      	{{-- <a href="resource/1.docx" class="tran3s"><i class="fa fa-file-word-o" aria-hidden="true"></i> Download doc</a> --}}

											      </div>
											    </div>
											  </div> <!-- /panel 3 -->
											  <div class="panel">
											    <div class="panel-heading">
											      <div class="panel-title">
											        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
											        	<table style="width: 100%;">
											        		<tr>
											        			<td>  UE 4 Manager l'opérationnel logistique et la qualité </td>
											        			<td><div class="p-color-bg">6</div></td>
											        			<td ><div class="p-color-bg">36</div></td>
											        		</tr>
											        	</table>
											        </a>
											      </div>
											    </div>
											    <div id="collapse4" class="panel-collapse collapse">
											      <div class="panel-body">
											      	<p>
											      		<ul>
											      		    <li><span class="text-list">Logistique et processus   (12 HRS CM) </span></li>
											      		    <li><span class="text-list">Innovation et créativité  (12 HRS CM)</span>  </li>
											      		    <li><span class="text-list">Qualité   (12 HRS CM) </span></li>
											      		</ul>
											      	</p>
											      	{{-- <a href="details-cours" class="tran3s"><i class="fa fa-angle-double-right" aria-hidden="true"></i> lire details cours</a> --}}
											      	{{-- <a href="resource/1.docx" class="tran3s"><i class="fa fa-file-word-o" aria-hidden="true"></i> Download doc</a> --}}

											      </div>
											    </div>
											  </div> <!-- /panel 4 -->
											  <div class="panel">
											    <div class="panel-heading">
											      <div class="panel-title">
											        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
											        	<table style="width: 100%;">
											        		<tr>
											        			<td>  UE 5 Manager les informations et le numérique</td>
											        			<td><div class="p-color-bg">3</div></td>
											        			<td ><div class="p-color-bg">72</div></td>
											        		</tr>
											        	</table>
											        </a>
											      </div>
											    </div>
											    <div id="collapse5" class="panel-collapse collapse">
											      <div class="panel-body">
											      	<p>
											      		<ul>
											      		    <li><span class="text-list">Recherches d'informations et informatique  (12 HRS TD)</span> </li>
											      		    <li><span class="text-list">Plan d'action opérationnel (24 HRS CM) </span> </li>
											      		    <li><span class="text-list">Systèmes d'information-ERP  (24 HRS CM)</span> </li>
											      		</ul>
											      	</p>
											      	{{-- <a href="details-cours" class="tran3s"><i class="fa fa-angle-double-right" aria-hidden="true"></i> lire details cours</a> --}}
											      	{{-- <a href="resource/1.docx" class="tran3s"><i class="fa fa-file-word-o" aria-hidden="true"></i> Download doc</a> --}}

											      </div>
											    </div>
											  </div> <!-- /panel 5 -->
											  <div class="panel">
											    <div class="panel-heading">
											      <div class="panel-title">
											        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
											        	<table style="width: 100%;">
											        		<tr>
											        			<td>  UE 6 Management responsable et cadre juridique</td>
											        			<td><div class="p-color-bg">3</div></td>
											        			<td ><div class="p-color-bg">48</div></td>
											        		</tr>
											        	</table>
											        </a>
											      </div>
											    </div>
											    <div id="collapse6" class="panel-collapse collapse">
											      <div class="panel-body">
											      	<p>
											      		<ul>
											      		    <li><span class="text-list">Droit des affaires  (24 HRS CM) </span></li>
											      		    <li><span class="text-list">Responsabilité sociétale et éthique (24 HRS CM) </span> </li>
											      		    <li><span class="text-list">Droit social (12 HRS CM) </span></li>
											      		</ul>
											      	</p>
											      	{{-- <a href="details-cours" class="tran3s"><i class="fa fa-angle-double-right" aria-hidden="true"></i> lire details cours</a> --}}
											      	{{-- <a href="resource/1.docx" class="tran3s"><i class="fa fa-file-word-o" aria-hidden="true"></i> Download doc</a> --}}

											      </div>
											    </div>
											  </div> <!-- /panel 6 -->
											  <div class="panel">
											    <div class="panel-heading">
											      <div class="panel-title">
											        <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
											        	<table style="width: 100%;">
											        		<tr>
											        			<td>  UE 7 Marketing-Entrepreneuriat et projet </td>
											        			<td><div class="p-color-bg">3</div></td>
											        			<td ><div class="p-color-bg">60</div></td>
											        		</tr>
											        	</table>
											        </a>
											      </div>
											    </div>
											    <div id="collapse7" class="panel-collapse collapse">
											      <div class="panel-body">
											      	<p>
											      		<ul>
											      		    <li><span class="text-list">Applied marketing  (24 HRS CM)</span> </li>
											      		    <li><span class="text-list">Entrepreneuriat  (12 HRS CM)</span>  </li>
											      		    <li><span class="text-list">Management de projets (12 HRS CM)</span> </li>
											      		</ul>
											      	</p>
											      	{{-- <a href="details-cours" class="tran3s"><i class="fa fa-angle-double-right" aria-hidden="true"></i> lire details cours</a> --}}
											      	{{-- <a href="resource/1.docx" class="tran3s"><i class="fa fa-file-word-o" aria-hidden="true"></i> Download doc</a> --}}

											      </div>
											    </div>
											  </div> <!-- /panel 7 -->

											</div> <!-- end #accordion -->
										</div> <!-- End of .course-curriculum-panel -->
	        						</div> <!-- /.single-panel-area -->

	        						<!-- ___________________________________ -->
	        						<div class="single-panel-area">
	        							<h6>SECOND SEMESTRE</h6>
	        							<div class="course-curriculum-panel">
											<div class="panel-group theme-accordion" id="accordionTwo">
											  <div class="panel">
											    <div class="panel-heading">
											      <div class="panel-title">
											        <a data-toggle="collapse" data-parent="#accordionTwo" href="#collapse13">
											        	<table style="width: 100%;">
											        		<tr>
											        			<td>UE 8 Projet Professionnel et Recherche  </td>
											        			<td><div class="p-color-bg">30</div></td>
											        			<td ><div class="p-color-bg">48</div></td>
											        		</tr>
											        	</table>
											        </a>
											      </div>
											    </div>
											    <div id="collapse13" class="panel-collapse collapse">
											      <div class="panel-body">
											      	<p>
											      		<ul>
											      		    <li><span class="text-list">Méthodologie PPR (8 HRS CM)</span></li>
											      		    <li><span class="text-list">Internship Battle (40 HRS TD) </span></li>
											      		    <li><span class="text-list">Mission pMission de professionnalisation (stage ou alternance) et mémoirerofessionnalisation </span></li>
											      		   
											      		</ul>
											      	</p>
											     {{--  	<a href="details-cours" class="tran3s"><i class="fa fa-angle-double-right" aria-hidden="true"></i> lire details cours</a> --}}
											      </div>
											    </div>
											  </div> <!-- /panel 2 -->


											</div> <!-- end #accordionTwo -->
										</div> <!-- End of .course-curriculum-panel -->
	        						</div> <!-- /.single-panel-area -->

	        					</div>
									  	</div>
									  	
									  </div>
									  	
									  </div>
									  {{-- Fin Div programme --}}
									  <div class="tab-pane" id="debouche">
									  		<h4 class="text-formation-title text-bold " style="margin-top: 20px; ">Métiers visés et débouchés</h4>
									  	<div class="text-justify" style="margin-top: 20px; margin-bottom: 10px;">
									  		<p> 
									  			 <ul>
									  			     <li><span class="text-list">Direction administrative et financière</span></li>
									  			     <li class="wow fadeInLeft" data-wow-delay="1s"><span class="text-list">Direction de grande entreprise ou d’établissement public</span></li>
									  			     <li class="wow fadeInLeft" data-wow-delay="2s"><span class="text-list">Direction de petite ou moyenne entreprise</span></li>
									  			     <li class="wow fadeInLeft" data-wow-delay="3s"><span class="text-list">Conseil en organisation et management d’entreprises</span></li>
									  			 </ul>
									  			<h4 class="text-formation-title text-bold  " style="margin-top: 20px; ">Secteurs d’activités</h4>
									  			 <ul>
									  			     <li><span class="text-list">Grandes Entreprises</span></li>
									  			     <li class="wow fadeInLeft" data-wow-delay="1s"><span class="text-list">ETI, PME, TPE</span></li>
									  			     <li class="wow fadeInLeft" data-wow-delay="2s"><span class="text-list">Collectivités locales et/ou territoriales</span></li>
									  			     <li class="wow fadeInLeft" data-wow-delay="3s"><span class="text-list">Institutions publiques....</span></li>
									  			 </ul>
									  			<h4 class="text-formation-title text-bold  " style="margin-top: 20px; ">Types d’emplois accessibles :</h4>
									  			 <ul>
									  			     <li><span class="text-list">Dirigeant de PME/PMI</span></li>
									  			     <li class="wow fadeInLeft" data-wow-delay="1s"><span class="text-list">Cadre supérieur en entreprise privée ou organisme public</span></li>
									  			     <li class="wow fadeInLeft" data-wow-delay="2s"><span class="text-list">Responsable administratif et financier</span></li>
									  			     <li class="wow fadeInLeft" data-wow-delay="3s"><span class="text-list">Gestionnaire d’établissements</span></li>
									  			     <li class="wow fadeInLeft" data-wow-delay="4s"><span class="text-list">Coordination de projets sociaux et socio-éducatifs</span></li>
									  			 </ul>
									  			<h4 class="text-formation-title text-bold  " style="margin-top: 20px; ">Poursuites des études</h4>
									  		</p>
									  		<p>
									  			Possibilité de doctorat avec l’IAE de Nice.
									  			 <ul>
									  			     <li><span class="text-list">http://unice.fr/iae/fr/recherche/le-laboratoire</span></li>
									  			     <li class="wow fadeInLeft" data-wow-delay="1s"><span class="text-list">http://unice.fr/iae/fr/recherche</span></li>
									  			     <li class="wow fadeInLeft" data-wow-delay="2s"><span class="text-list">http://sitere.science/</span></li>
									  			 </ul>
									  		</p>
									  		
									  	</div>

									  </div>
									</div>
	        				</div> <!-- /.single-event -->
	        			</div>
	        		</div> <!-- /.row -->
	        	</div> <!-- /.container -->
	        </div> <!-- /.event-section -->



			<!-- SubsCribe Banner ___________________ -->
	        <div class="subscribe-banner p-color-bg wow fadeInUp">
	        	<div class="container">
	        		<h3>postulez des maintenant</h3>
	        		<p>Faites votre admission au Master Direction D'Entreprises d&egrave;s aujourd'hui! </p>
	        		<a href="event" class="clear-fix">
	        			<button class="float-left tran3s wow fadeInRight">je postule</button>
	        		</a>
	        	</div>
	        </div>


@endsection