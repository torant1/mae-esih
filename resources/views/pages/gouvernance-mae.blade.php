@extends('layouts.master_home')
@section('content')
			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2 class="text-center">GOUVERNANCE</h2>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
					<ul>
						<li><a href="index">Accueil</a></li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>A propos</li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Gouvernance</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->

			<!-- Event Section _______________________ -->
	        <div class="gouvernace-section">
	        	<div class="container">
					<div class="row">
						<div class="text-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<p>
								Le Programme de <span class="text-red">MASTER EN ADMINISTRATION DES ENTREPRISES</span> Spécialité <span class="text-red">DIRECTION D’
								ENTREPRISE</span> de L’IAE de Nice délocalisé à Port-au-Prince, à travers un partenariat entre
								l’ESIH (Ecole Supérieure d’Infotronique d’Haïti), la Fondation Nationale pour l’Enseignement
								de la Gestion des Entreprises (FNEGE) et l’Institut d’Administration des Entreprises de
								l’Université de Nice, est sanctionné par le diplôme de l’IAE de Nice et de l’ESIH
							</p>

								<br>
								<p>
								Le programme est actuellement codirigé par <span class="text-bold">M. Patrick ATTIE</span>, Directeur Général, et
								<span class="text-bold">M. Jacques NORCEUS</span>, Directeur du Département de Gestion de l’ESIH.
								Ils sont assistés dans cette tâche par :
								<br>
								<br>
								<ul>
								    <li ><span class="text-bold text-list">Mme Marlène SAM</span>, Directrice de la Coopération Internationale et des Partenariats,</li>
								    <li class="wow fadeInLeft" data-wow-delay="1s"><span class="text-bold text-list">M. Yves BAH</span>, Chargé des affaires académiques et de la coordination</li>
								    <li class="wow fadeInLeft" data-wow-delay="2s"><span class="text-bold text-list">Dr Ben-Manson TOUSSAINT </span>, Directeur de la Recherche et du Laboratoire SITERE de l’ESIH.</li>
								</ul>
							</p>

								<br>
								<br>
								<p>
								Les orientations académiques sont décidées par un conseil qui définit le socle du contenu du
								programme avec des adaptations au contexte haïtien. La composition du conseil est la
								suivante :
								<br>
								<br>
								<ul>
								    <li ><span class="text-bold text-list ">Mme Nadine TOURNOIS</span> (IAE-Nice)</li>
								    <li class="wow fadeInLeft" data-wow-delay="1s"><span class="text-bold text-list">M. Davide SALOTTO</span> (IAE-Nice)</li>
								    <li class="wow fadeInLeft" data-wow-delay="2s"><span class="text-bold text-list">M. Arnaud LANGLOIS-MEURINNE </span> (FNEGE)</li>
								    <li class="wow fadeInLeft" data-wow-delay="3s"><span class="text-bold text-list">M. Patrick ATTIE</span> (ESIH)</li>	
								    <li class="wow fadeInLeft" data-wow-delay="4s"><span class="text-bold text-list">Mme Marlène SAM</span> (ESIH)</li>
								    <li class="wow fadeInLeft" data-wow-delay="5s"><span class="text-bold text-list">M. Yves BAH</span> (ESIH)</li>	
								    <li class="wow fadeInLeft" data-wow-delay="6s" ><span class="text-bold text-list">Dr Patrick CORSI</span></li>	
								    <li class="wow fadeInLeft" data-wow-delay="7s"><span class="text-bold text-list">Dr Ben-Manson TOUSSAINT</span>(SITERE-ESIH)</li>				
								</ul>

							</p>
								
						</div>
					    			
					</div>    		
	        	</div>
	        </div>
@endsection