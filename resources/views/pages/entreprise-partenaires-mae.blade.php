@extends('layouts.master_home')
@section('content')
			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2 class="text-center">CRÉEZ DE L'ÉMULATION, SUSCITEZ DES VOCATIONS...</h2>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
					<ul class="float-left">
						<li><a href="index">Accueil</a></li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Entreprises</li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Partenaires</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->

			<!-- Event Section _______________________ -->
	        <div class="partenaire-section wow fadeInUp">
	        	<div class="container">
					<div class="row">
						<div class="text-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<h2 class=" text-bold text-title">cr&eacute;ez de l'emulation et suscitez des vocations</h2>
							<p>
								L'interaction avec l’entreprise est un axe clé de notre projet pédagogique, dans le plus strict respect
									de l’esprit des IAE. La contextualisation des apprentissages et des problématiques sont une richesse
									que le MAE-DE recherche en permanence en créant des passerelles entre l’université et l’entreprise.
							</p>
							<p>
								Les entreprises publiques et privées d’Haïti sont donc chaleureusement invitées par l’équipe et par
								les partenaires du MAE-DE de Port-au-Prince à l’ESIH à venir partager leur expérience
								professionnelle avec l’institution et ses étudiant(e)s.
							</p>	
							 <br>
							 <p>
							 	En tant qu’acteurs du tissu économique, votre expérience et votre expertise représentent un
								précieux exemple pour nos étudiant(e)s et diplômé(e)s.
									
							 </p>
							 <p>
							 	Venez les partager à l'occasion d'un cours, d'une conférence ou d'un atelier. Vous pouvez aussi
								décider de faire partie d'un jury, de donner des conseils de perfectionnement, de formation, ou
								encore de participer à des groupes de réflexion sur l’insertion professionnelle des étudiant(e)s.
							 </p>	
						</div>
					    			
					</div>    		
	        	</div>
	        </div>
	           <div class="partenaire-utile-section wow fadeInUp">
	        	<div class="container">
					<div class="row">
						<div class="text-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<h2 class="text-bold text-title">un geste doublement utile</h2>
							<p>
								Pourquoi devenir partenaire du MAE-DE ?
								Votre participation permettra de :
								<ul>
								    <li> <span class="text-list"> Promouvoir votre métier, votre filière, votre entreprise (marque employeur)</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="1s"><span class="text-list"> Etre en contact direct avec les étudiants - vos potentiels collaborateurs de demain</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="2s"> <span class="text-list"> Partager vos points de vue, expériences et conseils sur un métier ou une filière</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="3s"><span class="text-list"> S'associer à une filière de formation d’excellence orientée management</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="4s"><span class="text-list"> Favoriser l’insertion des jeunes diplômé(e)s...</span></li>
								</ul>
							</p>
				
							<br>
							<p>
									Vous souhaitez bénéficier d’une veille scientifique et technologique et lancer une nouvelle
								dynamique d’innovation dans votre entreprise ? Vous pouvez nouer une relation privilégiée avec nos
								équipes de chercheurs ou d’enseignants.
								<br>

											<br>
								<h4 class="text-bold text-center" style="font-size: 20px; ">Le Programme MAE –DE met ses compétences académiques au service de votre projet</h4>
							
							</p>	
								
						</div>
					    			
					</div>    		
	        	</div>
	        </div>
	
	  
@endsection