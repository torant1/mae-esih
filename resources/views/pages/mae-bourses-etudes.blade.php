@extends('layouts.master_home')
@section('content')
			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2 class="text-center">VENEZ SOUTENIR L’EXCELLENCE</h2>
						<h4 class="text-inner-second">Investissons ensemble dans l’Ha&iuml;ti de demain</h4>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
					<ul class="float-left">
						<li><a href="index">Accueil</a></li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Etudiants</li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Financement</li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Bourses</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->

			<!-- Event Section _______________________ -->
	        <div class="partenaire-section wow fadeInUp">
	        	<div class="container">
					<div class="row">
						<div class="text-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
							{{-- <h2 class=" text-bold text-title">En soutenant LE MAE_ESIH</h2> --}}
							<p>
								Les initiateurs du MAE de l’ESIH ont souligné dès le début de leurs discussions la nécessité de la mise en place d’un dispositif d’aides financières à l’intention des étudiant(e)s du programme. Ces aides ont pour principal objet d’apporter un soutien décisif aux bénéficiaires pour :
								<br>
								<br>
								<ul>
								    <li ><span class=" text-list">Permettre la participation de jeunes femmes et hommes sur la base de l’excellence et de critères sociaux</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="1s"><span class=" text-list">Faciliter l’accès d’étudiant(e)s méritants issu(e)s de foyers modestes ou de milieux défavorisés à un niveau élevé de l’enseignement supérieur</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="2s"><span class=" text-list"> Impulser l’innovation managériale en Haïti en vue d’augmenter la productivité et les performances des entreprises et organisations publiques et privées</span></li>
								    <li class="wow fadeInLeft" data-wow-delay="3s"><span class=" text-list">Favoriser la réussite d’études dans un domaine crucial, de pointe et en évolution</span></li>
								     <li class="wow fadeInLeft" data-wow-delay="4s"><span class=" text-list"> Répondre à des situations exceptionnelles ou à des besoins émanant de domaines socio-économiques prioritaires au plan national et régional</span></li>
								</ul>
							</p>
						<br>
							<P>
							<h4 class="text-bold text-center" style="font-size: 20px; ">Elles ne se substituent pas aux bourses ou aux aides existantes par ailleurs.</h4>
							</P>
						</div>
					    			
					</div>    		
	        	</div>
	        </div>
	  
@endsection