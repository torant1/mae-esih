@extends('layouts.master_home')
@section('content')
			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2 class="text-center">PAS DE DÉVELOPPEMENT CONTINU SANS FORMATION</h2>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
					<ul class="float-left">
						<li><a href="index">Accueil</a></li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Entreprises</li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Former vos collaborateurs</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->

			<!-- Event Section _______________________ -->
	        <div class="collaborateurs-section wow fadeInUp">
	        	<div class="container">
					<div class="row">
						<div class="text-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<h2 class=" text-bold text-title"> Pas de d&eacute;veloppement continue sans formation</h2> 
							<p>
								Parce que dans la vie de l'entreprise, il y a toujours à apprendre, le programme MAE-DE vous
								propose un programme ouvert aux professionnels, à vos cadres de direction pour les former ...
								ou à vous-même, pour un perfectionnement ou une mise à jour( ???).
								
							</p>
							<br>
							<p>
								Dans un environnement en constante et rapide évolution, une entreprise a toujours besoins
								d'améliorer sa performance et celle de ses équipes. C'est dans cette optique que le programme vous
								propose une solution de formation collective et individuelle :
								<br>
								<ul>
								    <li> <span class="text-list"> Pour entraîner les équipes dans le développement de leurs compétences et leur mise en pratique </span></li>
								    <li class="wow fadeInLeft" data-wow-delay="1s"> <span class="text-list"> Pour anticiper les besoins de l’organisation et des individus qui la composent </span></li>
								    <li class="wow fadeInLeft" data-wow-delay="2s"><span class="text-list"> Pour garantir ensemble l'efficacité des dispositifs mis en oeuvre.</span></li>							
								</ul>

							</p>
								
						</div>
					    			
					</div>    		
	        	</div>
	        </div>
	        <div class="besion-collaborateurs-section wow fadeInUp">
	        	<div class="container">
					<div class="row">
						<div class="text-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<h2 class="text-bold text-title">Quels dispositifs pour quels besoins?</h2>
							<p>
								Vous vous demandez si le programme est fait pour vous ou comment concilier travail, vie
									personnelle et reprise d’études en toute sérénité ?
									Nos équipes vous accompagnent dans le développement d’une offre adaptée à la stratégie de votre
									entreprise, que ce soit sur les thématiques du management, de la stratégie, du leadership, de la
									finance, des ressources humaines ou de la gestion.
									</p>
									<br>
									<p>
								<h4 style="text-align: center; "><a href="contact-mae-esih">Demandez le programme <i class="fa fa-long-arrow-right fa-link wow wobble" data-wow-duration="5s"></i></a></h4>

							</p>
								
								
						</div>
					    			
					</div>    		
	        	</div>
	        </div>
	
	  
@endsection