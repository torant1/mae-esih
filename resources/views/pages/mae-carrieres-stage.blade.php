@extends('layouts.master_home')
@section('content')
			<!-- Inner Page Main Banner __________________ -->
			<div class="inner-page-banner">
				<div class="opacity">
					<div class="container">
						<h2 class="text-center">stages dans nos entreprises partenaires</h2>
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.inner-page-banner -->


			<!-- Page Breadcrum __________________________ -->
			<div class="page-breadcrum">
				<div class="container">
						<ul>
						<li><a href="index">Accueil</a></li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>Espace  Carri&egrave;res</li>
						<li><i class="fa fa-caret-right"></i></li>
						<li>stages</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.page-breadcrum -->

			<!-- Event Section _______________________ -->
	        <div class="recherche-section wow fadeInUp">
	        	<div class="container">
					<div class="row">
						<div class="text-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<p>
								MAE_ESIH ambitionne de construire un maillage national et de développer un réseau dense à l’international avec un grand nombre d’entreprises, d’institutions et d’employeurs afin de faciliter la recherche de stages et d’emploi. Une stratégie agressive de placement et de recherche de débouchés pour des stages à forte valeur ajoutée aussi bien pour l’étudiant que pour la structure d’accueil {{-- <a href="#"><span>Emploi</span> </a> --}}
							</p>
							<br>	
							 <br>
								<p class="list-offre">
								<span class="offre-stage"> <a href="#">Consulter les offres de stage <i class="fa fa-long-arrow-right fa-link wow wobble" ></i></a></span>	
							</p>
						</div>
					    			
					</div>    		
	        	</div>
	        </div>
	  
@endsection