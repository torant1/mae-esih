  <!-- Footer ______________________________ -->
	        <footer>
	        	<div class="top-footer">
	        		<div class="container">
	        			<div class="row">
	        				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 footer-about">
	        					<h4>A PROPOS </h4>
	        					{{-- <p><span class="mae-esih"></span> Mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the sys- tem, and expound the actual teachings of the great explorer</p> --}}
	        					<a  href="mots-de-la-direction" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i>Mots de la direction</a>
	        					<a  href="vision-mission-mae" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i>Vision-mission</a>
	        					<a  href="gouvernance-mae" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i>Gouvernance</a>
	        					<a  href="mae-partenaires" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i>Partenaires</a>
	        					
	        					<ul>
	        						<h5 style="color: #d74848;">Notre pr&eacute;sence sur les r&eacute;seaux sociaux</h5>
									<li class="wow fadeInUp" ><a  href="#" class="tran3s round-border icon"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			        				<li class="wow fadeInUp" data-wow-delay="0.5s"><a  href="#" class="tran3s round-border icon"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li class="wow fadeInUp" data-wow-delay="1s"><a  href="#" class="tran3s round-border icon"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
									<li class="wow fadeInUp" data-wow-delay="1.5s"><a  href="#" class="tran3s round-border icon"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
								</ul>
	        				</div> <!-- /.footer-about -->

	        				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 footer-contact">
	        					<h4>Pour nous Contacter</h4>
	        					<ul>
	        						<li class="wow fadeInDown">
	        							<i class="fa fa-envelope-o" aria-hidden="true"></i>
	        							<a  href="mailto:mae@esih.edu" class="tran3s">mae@esih.edu</a>
	        						</li>
	        						<li>
	        							<i class="fa fa-phone" aria-hidden="true"></i>
	        							<a  href="tel:+50922264749" class="tran3s">+509 22 26 47 49</a>
	        						</li>
	        						<li><i class="fa fa-map-marker" aria-hidden="true"></i><a class="text-center">29, 2e ruelle Nazon, Bourdon, Port-au-Prince, Ha&iuml;ti.</a></li>
	        						<li class="wow fadeInUp"><i class="fa fa-hand-o-right" aria-hidden="true"></i> <a  href="contact-mae-esih" class="tran3s"> Formulaire de contact</a></li>
	        					</ul>
	        				</div> <!-- /.footer-contact -->

	        				<div class="col-lg-5 col-md-6 col-sm-6 col-xs-12 footer-quick-link">
	        					<h4>Liens rapides</h4>
	        					<div class="col-lg-6">
	        					<ul>
	        						<li><a  href="formation-direction-entreprise" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i> La Formation</a></li>
	        						<li><a  href="espace-etudiant-mae" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i> Espace Etudiant(e)s</a></li>
	        						<li><a  href="mae-bourses-etudes" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i>Bourses-Financement</a></li>
	        						<li><a  href="page-under-construction" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i>Biblioth&egrave;que virtuelle </a></li>
	        						<li><a  href="mae-carrieres-emploi" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i> Espace  Carri&egrave;res</a></li>
	        						<li><a  href="page-under-construction" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i> Les Anciens</a></li>
	        						<li><a  href="page-under-construction" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i> Forums Entreprises</a></li>
	        					</ul>
	        					</div>
	        					<div class="col-lg-6">
	        					<ul>
	        						<li><a  href="evenements" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i>Ev&egrave;nements</a></li>
	        						<li><a  href="actualites" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i>Actualit&eacute;s</a></li>
	        						<li><a  href="medias-galerie" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i> M&eacute;dias</a></li>
	        						{{-- <li><a  href="faq" class="tran3s text-uppercase"><i class="fa fa-caret-right" aria-hidden="true"></i> Faq</a></li> --}}
	        						{{-- <li><a  href="page-under-construction" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i> Help</a></li> --}}
	        					</ul>
	        					</div>
	        				</div> <!-- /.footer-quick-link -->
	        				
	        	
	        			</div> <!-- /.row -->
	        		</div> <!-- /.container -->
	        	</div> <!-- /.top-footer -->

	        	<div class="bottom-footer">
	        		<p>Copyright 2017 &copy; <a  href="#!" class="tran3s" >MAE ESIH</a> <span>|</span> R&eacute;alis&eacute; par <a  href="http://www.monoingroup.com"><span class="p-color">MONOIN GROUP</span></a></p>
	        	</div> <!-- /.bottom-footer -->
	        </footer>

	        <!-- Scroll Top Button -->
			<button class="scroll-top tran3s p-color-bg">
				<i class="fa fa-angle-up" aria-hidden="true"></i>
			</button>
			<!-- pre loader  -->
		 	<div id="loader-wrapper">
				<div id="loader"></div>
			</div>