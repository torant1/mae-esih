<!-- Header _________________________________ -->
			<header >

				<div class="top-header "  style="background-color: #fff;">
					<div class="container">
						<div class="logo-iae col-lg-4 col-md-4 col-10-xs-4">
						
						<a href="http://unice.fr/iae/fr/lecole/accueil" target="_blank"><img src="{{asset('images/logo/iae/logo_iae_nice.png')}}" alt="logo-iae-nice"></a>	
						</div> <!-- /.left-side -->
						<div class="logo-esih col-lg-4 col-md-4 col-10-xs-4">
						<a href="http://www.esih.edu/"  target="_blank"><img src="{{asset('images/logo/esih/EsihMetallique_ESIH IEEE_160x100.png')}}" alt="logo-Ecole Superieure-infotronique-haiti"></a>
						</div>	 <!-- /.right-side -->
						<div class="logo-fnege col-lg-4 col-md-4 col-10-xs-4">
						<a href="http://www.fnege.org"  target="_blank"><img src="{{asset('images/logo/fnege/logo_1_fnege.png')}}" alt="logo-fnege"></a>	
						</div> <!-- /.right-side -->
					</div>
				</div> <!-- /.top-header -->

				<!-- _______________________ Theme Menu _____________________ -->


				<div class="head-menu">
					<div class="main-menu-wrapper clear-fix">
						<div class="container">
						
							
							<!-- Menu -->
							
							<nav class="navbar float-right">
							   <!-- Brand and toggle get grouped for better mobile display -->
							   <div class="navbar-header">
							     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
							       <span class="sr-only">Toggle navigation</span>
							       <span class="icon-bar"></span>
							       <span class="icon-bar"></span>
							       <span class="icon-bar"></span>
							     </button>
							   </div>
							   <!-- Collect the nav links, forms, and other content for toggling -->
							   <div class="collapse navbar-collapse" id="navbar-collapse-1">
							     <ul class="nav navbar-nav">
							     	 <li class="dropdown-holder current-page-item"><a  href="index">Accueil</a>
							       <li class="dropdown-holder "><a  >a propos</a>
										<ul class="sub-menu">
											<li><a  href="mots-de-la-direction" class="tran3s">mots de la direction</a></li>
											<li> <a  href="vision-mission-mae" class="tran3s">Vision-mission</a> </li>
											<li><a  href="gouvernance-mae" class="tran3s">Gouvernance</a></li>
											<li><a  href="mae-partenaires" class="tran3s">Partenaires</a></li>
										{{-- <li><a  href="faq" class="tran3s">FAQ</a></li> --}}	
										</ul>
							       </li>
							       <li class="dropdown-holder"><a >Formation</a>
							       	<ul class="sub-menu">
							       	    <li><a target="_blank"  href="formation-direction-entreprise" class="tran3s">MAE Direction d'Entreprises</a></li>
							       	    <li><a  href="recherche-mae" class="tran3s">Recherche</a></li>
							       	    <li><a  href="page-under-construction" class="tran3s">corps Enseignant</a></li>
							       	</ul>
							       </li>
							       <li class="dropdown-holder"><a  >Etudiant(e)s</a>
										<ul class="sub-menu">
											<li><a  href="espace-etudiant-mae" class="tran3s">Espace Etudiant(e)s</a></li>
											  <li ><a  href="page-under-construction" class="tran3s">financement</a>
											  	 <ul class="sub-submenu" >
											  	    <li><a  href="mae-bourses-etudes" class="tran3s">Bourses</a></li>
											  	    <li><a  href="page-under-construction" class="tran3s">Prets</a></li>
											  	   {{--  <li><a  href="page-under-construction" class="tran3s">Sponsoring</a></li> --}}
											  	    <li><a  href="page-under-construction" class="tran3s">Autres</a></li>
											  	</ul> 
											  </li>
											<li><a  href="page-under-construction" class="tran3s">Espace des Anciens</a></li>
											<li><a  href="page-under-construction" class="tran3s">Bibliotheque virtuelle</a></li>
										</ul>
							       </li>
							       <li class="dropdown-holder"><a  href="#">Entreprises</a>
										<ul class="sub-menu">
											<li><a  href="entreprise-partenaires-mae" class="tran3s"> Partenaires</a></li>
											<li><a  href="page-under-construction" class="tran3s"> ateliers-rencontres-conferences</a></li>
											<li><a  href="formations-collaborateurs-mae" class="tran3s">Former vos collaborateurs </a></li>
											<li><a  href="page-under-construction" class="tran3s">Forum Entreprises</a></li>
										</ul>
							       </li>
							       <li class="dropdown-holder"><a  href="#">Actualites</a>
										<ul class="sub-menu">
											<li><a  href="actualites" class="tran3s">Actualites</a></li>
											<li><a  href="evenements" class="tran3s">Evenements</a></li>
											<li><a  href="medias-galerie" class="tran3s">medias</a></li>
											
										</ul>
							       </li>
							       <li class="dropdown-holder"><a > espace Carrieres</a>
							       		<ul class="sub-menu">
											<li><a  href="mae-carrieres-stage" class="tran3s">Stages</a></li>
											<li><a  href="mae-carrieres-emploi" class="tran3s">Emploi</a></li>
										</ul>
							       </li>
							       <li><a  href="contact-mae-esih">contact</a></li>
							     </ul>
							   </div><!-- /.navbar-collapse -->
							</nav>
						</div>
					</div> <!-- /.main-menu-wrapper -->
				</div>
			</header>

