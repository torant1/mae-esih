<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<!-- For IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- For Resposive Device -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="csrf-token" content="{{csrf_token()}}">
		<title>{{ config('app.name','mea_esih')}}</title>

		<!-- Favicon -->
		<link rel="icon" type="image/png" sizes="56x56" href="{{asset('images/fav-icon/icon.png')}}">


		<!-- Main style sheet -->
		<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css')}}">
		<!-- responsive style sheet -->
		<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css')}}">
	{{-- 	<link rel="stylesheet" type="text/css" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css')}}"> --}}
		<!-- Fix Internet Explorer ______________________________________-->

		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script src="vendor/html5shiv.js"></script>
			<script src="vendor/respond.js"></script>
		<![endif]-->
			
	</head>

	<body>
		@include('layouts.partials.navbar_master')
		@yield('content')
		@include('layouts.partials.footer_master')
		


		<!-- Js File_________________________________ -->

		<!-- j Query -->
		<script type="text/javascript" src="{{asset('vendor/jquery-2.1.4.js')}}"></script>

		<!-- Bootstrap JS -->
		<script type="text/javascript" src="{{asset('vendor/bootstrap/bootstrap.min.js')}}"></script>

		<!-- Vendor js _________ -->
		<!-- revolution -->
		<script src="{{asset('vendor/revolution/jquery.themepunch.tools.min.js')}}"></script>
		<script src="{{asset('vendor/revolution/jquery.themepunch.revolution.min.js')}}"></script>
		<script type="text/javascript" src="{{asset('vendor/revolution/revolution.extension.slideanims.min.js')}}"></script>
		<script type="text/javascript" src="{{asset('vendor/revolution/revolution.extension.layeranimation.min.js')}}"></script>
		<script type="text/javascript" src="{{asset('vendor/revolution/revolution.extension.navigation.min.js')}}"></script>
		<script type="text/javascript" src="{{asset('vendor/revolution/revolution.extension.kenburn.min.js')}}"></script>
		<script type="text/javascript" src="{{asset('vendor/revolution/revolution.extension.actions.min.js')}}"></script>

		<!-- Google map js -->
		<script src="{{ asset('http://maps.google.com/maps/api/js?key=AIzaSyA5oVKHQAApeR0nUEWAX7sk1_nCV6-vtJk')}}"></script> <!-- Gmap Helper -->
{{-- 		<script src="{{asset('vendor/gmap.js')}}"></script>
 --}}		<!-- Bootstrap Select JS -->
{{-- 		<script type="text/javascript" src="{{asset('vendor/bootstrap-select/dist/js/bootstrap-select.js')}}"></script>
 --}}		<!-- Time picker -->
{{-- 		<script type="text/javascript" src="{{asset('vendor/time-picker/jquery.timepicker.min.js')}}"></script>
 --}}		<!-- WOW js -->
		<script type="text/javascript" src="{{asset('vendor/WOW-master/dist/wow.min.js')}}"></script>
		<!-- owl.carousel -->
		<script type="text/javascript" src="{{asset('vendor/owl-carousel/owl.carousel.min.js')}}"></script>
		<!-- js count to -->
{{-- 		<script type="text/javascript" src="{{asset('vendor/jquery.appear.js')}}"></script>
 --}}{{-- 		<script type="text/javascript" src="{{asset('vendor/jquery.countTo.js')}}"></script>
 --}}		<!-- Validation -->
	<script type="text/javascript" src="{{asset('vendor/contact-form/validate.js')}}"></script>
		<script type="text/javascript" src="{{asset('vendor/contact-form/jquery.form.js')}}"></script>

		<!-- Theme js -->
		<script type="text/javascript" src="{{asset('js/theme.js')}}"></script>

	 <!-- /.main-page-wrapper -->

		<script>
$('#myTab a').click(function (e) {
  e.preventDefault();
  $(this).tab('show');
})

// $('.closeAlert').click(function (){
//   $('.contact-us-form').load(location.href +".contact-us-form>*","");
// })
</script>
	</body>
</html>
