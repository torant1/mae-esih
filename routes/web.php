<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
  Route::get('/{slug?}', 'PagesController@show')->name('root_path');

//Route::get('/{pages?}', 'PagesController@index')->name('root_path');

// Route::resources(['/{pagesname}'=>'PagesController']);

// Route::resource('/{pagesname?}','PagesController');

//Route::get("/{pagesname}", "PagesController@show")->name('pages_name');


// Route::resource('pages/{name?}', 'PagesController',['parameters'=>[
// 				'pages'=>'Pages']]);


// Route::redirect('/', '/pages', 301);
Auth::routes();

 